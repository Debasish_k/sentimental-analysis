�]q (X   scatologicalqX   cutesyqX   southernqX   tchaikovskyqX   head-turnerqX
   heidegger-qX   contestqX   possibleqX   cinematographyq	X   inferiorq
X   choppyqX	   'childrenqX   self-parodyqX   giganticqX   wrappedqX   action-thriller/darkqX   bottom-feederqX
   punishableqX   murder-suicideqX   firstqX   furiousqX
   attractiveqX   straightforwardqX   stickyqX   non-threateningqX   tykwerqX
   guilt-freeqX   weakqX   too-conscientiousqX   pinochetqX   cannedqX   documentary-likeq X   perspicaciousq!X   holisticq"X
   scientificq#X   boyq$X   often-funnyq%X
   humor�itq&X	   messianicq'X   awkwardq(X   half-badq)X
   newfangledq*X   introspectiveq+X   objectionableq,X   'bestq-X
   girl-womanq.X   art-directedq/X   half-an-hourq0X   nobleq1X
   slash-dashq2X   dormantq3X
   r�sum�q4X   hopkins/rockq5X   howardq6X   docileq7X   lyricq8X   broadq9X   greek-americanq:X   cladq;X   uruk-haiq<X   'beenq=X   surestq>X   savviestq?X   dongq@X   airqAX   reliableqBX   utterlyqCX
   hyperbolicqDX   damon/bourneqEX   childishqFX   organqGX   sexyqHX   ear-pleasingqIX   rareqJX   not-so-brightqKX   mass-murderingqLX   white-trashqMX   diverseqNX
   derivativeqOX   respiteqPX	   'terribleqQX   troubledqRX	   antitrustqSX   holdqTX   embarrassingqUX   silly-lookingqVX   venomousqWX   blasphemousqXX	   93-minuteqYX   masochisticqZX   beat-chargedq[X   criticq\X	   arbitraryq]X	   electoralq^X   dead-centerq_X   self-reflexiveq`X   off-the-wallqaX   spell-castingqbX   frat-boyqcX   non-stopqdX   imaginativeqeX   space-basedqfX   rockumentaryqgX   elusiveqhX	   knee-jerkqiX   slowqjX
   definitiveqkX   tweedyqlX   non-believerqmX   prep-schoolqnX   'dateqoX   readyqpX   maleqqX   message-mongeringqrX   boy-meets-girlqsX   it's-surrealqtX   silentquX   outreqvX   neo-noirqwX   lowerqxX   unselfconsciousqyX	   meet-cuteqzX   palestinianq{X	   unfocusedq|X   under-inspiredq}X   largeq~X   stinkyqX   'thankq�X   mimeticq�X
   small-townq�X   kitschyq�X   kids-and-family-orientedq�X   humbleq�X   operationalq�X
   irrelevantq�X   lovelyq�X   iranian-americanq�X   morbidq�X   /bq�X   apparentq�X   small-scaleq�X   kathleenq�X   bartlebyq�X   learq�X   lip-synchingq�X   schtickyq�X   playfulq�X   larger-than-lifeq�X   roller-coasterq�X   aerialq�X
   infectiousq�X   suburbanq�X   razor-sidedq�X   fertileq�X   middle-agedq�X	   clayburghq�X	   40-minuteq�X   significantq�X   seventy-minuteq�X   made-for-movieq�X
   responsiveq�X   keep-'em-guessingq�X   cinema-and-selfq�X   madq�X   extraq�X   germanicq�X	   startlingq�X
   personableq�X   fear-inducingq�X	   'shindlerq�X   subjectq�X   digq�X
   toe-to-toeq�X   somnambulantq�X   standardizedq�X   botherq�X   austereq�X   self-inflictedq�X   near-disasterq�X   walking-deadq�X   crowd-pleaserq�X	   incessantq�X   unstableq�X   anne-sophieq�X   lip-non-synchingq�X	   long-heldq�X   long-windedq�X   inhalantq�X   untoldq�X   single-handedq�X   coolq�X   headline-freshq�X   thirdq�X
   unlikeableq�X   movie-goingq�X   worthyq�X   movie-esqueq�X   messyq�X   brotherq�X   b-movieq�X	   imbecilicq�X   well-constructedq�X   provocativeq�X   fifteen-minuteq�X   pentacostalq�X   collectibleq�X   monasticq�X   near-masterpieceq�X   post-modernq�X   gui�nq�X	   miserableq�X   squanderq�X	   elizabethq�X   irreversibleq�X
   interestedq�X   well-formedq�X   stage-trainedq�X   strikingq�X	   desperateq�X   existentialq�X
   best-knownq�X	   overgrownq�X   'hungry-manq�X
   precariousq�X   demographicq�X   agileq�X   'innovativeq�X
   dislikableq�X   bibleq�X   sieveq�X   self-satisfiedq�X   movie-starringq�X   3-dq�X   postapocalypticq�X
   tumultuousq�X
   26-episodeq�X   suitableq�X   harryq�X   pg-13-ratedq�X	   establishq�X   well-realizedq�X   lagaanq�X   traditionalq�X   'urbanq�X   vividq�X   morvernq�X
   meditativeq�X   sub-parq�X   redneck-versus-bluebloodq�X	   brilliantq�X   broken-downq�X   temperamentalq�X   world-wearyq�X   21stq�X   earthyq�X   time-honoredr   X   wearr  X   tortuousr  X
   alchemicalr  X   mirthr  X
   top-billedr  X   tongue-tiedr  X   estherr  X   much-neededr  X   double-barreledr	  X	   79-minuter
  X   glad-handingr  X   fear-reducingr  X   headyr  X   third-personr  X   keyr  X   lamer  X	   strongestr  X   kungr  X   bernalr  X   bad-boyr  X	   timeframer  X   graciousr  X	   reel/realr  X   rabbit-proofr  X   narrowr  X
   not-so-hotr  X   'plexr  X	   confidentr  X   shoddyr  X   afreshr  X   time-wastingr  X	   tenaciousr   X   rowdyr!  X   no-surpriser"  X   rosemaryr#  X
   off-centerr$  X   futurer%  X   hit-and-missr&  X	   tavernierr'  X   'whiter(  X   ill-wroughtr)  X   imposterr*  X	   promisingr+  X   wackyr,  X
   off-handedr-  X   enthusiasticr.  X   tamer/  X   way-coolr0  X   pivotalr1  X
   half-assedr2  X   �anr3  X   piler4  X   claustrophobicr5  X   anarchyr6  X   allegedr7  X   truth-in-advertisingr8  X   manicr9  X   awe-inspiringr:  X   durabler;  X   evasiver<  X	   committedr=  X
   worthwhiler>  X   floridr?  X   too-longr@  X   exoticrA  X
   half-bakedrB  X   thirty-threerC  X	   hypertimerD  X   phoney-feelingrE  X   quasi-documentaryrF  X   finalrG  X   starkrH  X   comparativerI  X   'erJ  X   sungrK  X   polishedrL  X   densestrM  X   creakyrN  X	   feminizedrO  X   strictrP  X   resourcefulrQ  X
   spider-manrR  X
   poster-boyrS  X   coralrT  X   by-the-numbersrU  X   crediblerV  X   youngrW  X   cameo-packedrX  X   insubstantialrY  X   strZ  X
   talanc�nr[  X   give-a-damnr\  X   liberalr]  X   smearyr^  X
   defensibler_  X   unpersuasiver`  X
   unromanticra  X   hormonalrb  X   commendablerc  X
   insatiablerd  X   inauspiciousre  X   upper-crustrf  X   craftrg  X   foulrh  X   not-very-funnyri  X	   obnoxiousrj  X   10thrk  X   so-sorl  X   cooperrm  X   beliefrn  X	   effectivero  X   22-year-oldrp  X   sorryrq  X
   commentaryrr  X   romantic-comedyrs  X   semi-throwbackrt  X   'tadpoleru  X	   universalrv  X   punitiverw  X   heavy-handedrx  X   tommyry  X   ethnographicrz  X   rogerr{  X   beat-the-clockr|  X   autobiographicalr}  X   usedr~  X   haroldr  X   large-screenr�  X   premierr�  X   femaler�  X   wordlessr�  X   retrospectiver�  X
   forgivabler�  X   ditsyr�  X   poignantr�  X   loudr�  X   two-dimensionalr�  X
   unexpectedr�  X   fullr�  X
   easy-goingr�  X	   formulaicr�  X   worser�  X   moorer�  X   usefulr�  X   orqu�deasr�  X
   rebelliousr�  X   no-holds-barredr�  X
   negligibler�  X   'amateurr�  X   philosophicalr�  X   inter-racialr�  X   13thr�  X   splashr�  X   fantasy-adventurer�  X   roter�  X   wide-rangingr�  X   heavenr�  X
   snail-liker�  X   jewishr�  X   self-destructiver�  X   boilerplater�  X   two-wayr�  X   bigr�  X   'coolr�  X   fallibler�  X	   laughabler�  X   conspicuousr�  X   hearstr�  X	   tarkovskyr�  X   x-menr�  X   spielbergianr�  X   funkyr�  X   limitedr�  X   hmmm�mightr�  X   cheaperr�  X   good-humoredr�  X
   gender-warr�  X   water-boundr�  X   subtler�  X   hispanicr�  X   deviousr�  X   naughtyr�  X   freakishr�  X   'edgyr�  X   b-filmr�  X   valuabler�  X
   triumphantr�  X   t�mr�  X   eroti-comedyr�  X   up-and-comingr�  X   sympathyr�  X   deeperr�  X   self-involvedr�  X   point-and-shootr�  X
   mechanicalr�  X   anemicr�  X
   subversiver�  X   watered-downr�  X   vaguer�  X   merchant-ivoryr�  X   binaryr�  X   acuter�  X   'german-expressionistr�  X   immuner�  X   munchausen-by-proxyr�  X
   individualr�  X	   drawn-outr�  X   infantilizedr�  X   tap-dancingr�  X   absurdr�  X   jean-clauder�  X   accurater�  X	   nostalgicr�  X   septr�  X	   chocolater�  X   preposterousr�  X   singularr�  X
   anti-harryr�  X   little-knownr�  X   time-consumingr�  X	   prewarnedr�  X   filmicr�  X	   dependentr�  X   ostentatiousr�  X	   sha-na-nar�  X	   acclaimedr�  X   hit-or-missr�  X   totalitarianr�  X
   parmentierr�  X   genuinamenter�  X	   insistentr�  X   toe-tappingr�  X   three-to-oner�  X
   monumentalr�  X
   real�sher�  X   paint-by-numbersr�  X	   dystopianr�  X   dead-undeadr�  X   downbeatr�  X   derangedr�  X   play-dohr�  X
   re-workingr�  X   oscar-nominatedr�  X   difficult-to-swallowr�  X   mediterraneanr�  X   tightr�  X   double-crossr�  X   zealousr�  X   counterproductiver�  X   kiddie-flickr�  X   easyr�  X   primer�  X   scriptr�  X   soldierr�  X   lame-oldr�  X	   expectantr   X   comfortabler  X   well-behavedr  X   unavailabler  X   technology-of-the-momentr  X   prosaicr  X   eternalr  X	   desultoryr  X   class-r  X	   rosenthalr	  X   easierr
  X
   even-tonedr  X   post-feministr  X   rememberr  X	   difficultr  X	   parochialr  X	   65-minuter  X   snazzy-lookingr  X   hoaryr  X   arriver  X   reminiscentr  X   'fishr  X   cross-countryr  X   unrecommendabler  X   homericr  X   wryr  X   food-spittinglyr  X   conspiratorialr  X   fasterr  X   channel-styler  X   grainyr  X   identity-seekingr  X	   manhattanr   X   repetitiousr!  X   psychologicalr"  X   indianr#  X   nextr$  X   majority-orientedr%  X   unawarer&  X   panicr'  X   relativer(  X   concealr)  X   �direct-to-videor*  X   middle-classr+  X   facialr,  X   self-congratulatoryr-  X
   tantamountr.  X   4everr/  X   rom�nticasr0  X   sitcom-cuter1  X   psychicr2  X   accident-proner3  X   bigelowr4  X   wistfulr5  X   shoot-em-upr6  X   creepy-crawlyr7  X   forcefulr8  X   yiddishr9  X   slatherr:  X	   automaticr;  X   rushedr<  X
   particularr=  X   over-amorousr>  X   major-leaguer?  X   tributer@  X   anti-warrA  X
   rocky-likerB  X   greedyrC  X   daverD  X   grunge-piraterE  X   cleanerrF  X
   unfilmablerG  X   cop-outrH  X   run-of-the-filthrI  X   irreconcilablerJ  X   conceivablerK  X
   two-fifthsrL  X   unaccountablerM  X   combustiblerN  X   oh-so-importantrO  X   jointrP  X   skittishrQ  X   entrepreneurialrR  X	   revealingrS  X   filledrT  X   nine-year-oldrU  X	   well-maderV  X   dialogue-heavyrW  X   20thrX  X   snap-cracklerY  X	   arthriticrZ  X   'ther[  X	   strenuousr\  X   erraticr]  X   twenty-somer^  X	   chemistryr_  X   two-hourr`  X   errantra  X   feralrb  X   hardheartedrc  X   'howrd  X   hollywood-izedre  X   tongue-in-cheekrf  X   deepestrg  X   sports-movierh  X	   narrativeri  X   'naturerj  X   well-meaningnessrk  X   african-americanrl  X   matter-of-factrm  X   validrn  X   'significantro  X   heart-poundingrp  X   lesser-praisedrq  X   inexplicablerr  X   avidrs  X   19th-centuryrt  X	   four-starru  X   run-of-the-millrv  X   closerrw  X   zippyrx  X   eagerry  X   indecipherablerz  X   guzm�nr{  X
   holofcenerr|  X   lumpenr}  X   regularr~  X	   grotesquer  X	   psychoticr�  X   altman-esquer�  X   tailor-mader�  X   deeplyr�  X   edge-of-your-seatr�  X   blastr�  X	   infantiler�  X   animated-movier�  X   cursoryr�  X   mysticr�  X
   headbangerr�  X   m�nchr�  X   artyr�  X   rarestr�  X
   reflectiver�  X   intelligibler�  X   zero-dimensionalr�  X
   girl-buddyr�  X   cold-heartedr�  X   minor-leaguer�  X   49-year-oldr�  X   beer-soakedr�  X   niftiestr�  X   must-ownr�  X   open-mindedr�  X   anti-eroticr�  X   post-breakupr�  X   low-wattager�  X   familiarr�  X   holiday-seasonr�  X	   applegater�  X   kiddie-friendly�r�  X   well-structuredr�  X   big-heartedr�  X   paranoidr�  X   inconformidadr�  X   loftyr�  X   boom-boxr�  X   deepr�  X   suspensefulr�  X   buffr�  X   'myr�  X	   recessiver�  X   non-exploitiver�  X   a-teamr�  X   subordinater�  X   seriousr�  X   brutalr�  X	   territoryr�  X   alliedr�  X	   wonderousr�  X   supernaturalr�  X   three-dimensionalr�  X   self-sacrificer�  X   mustyr�  X   pre-dawnr�  X
   paranormalr�  X   community-colleger�  X   worstr�  X	   ingeniousr�  X   'ahr�  X   old-timer�  X   burstr�  X   exactr�  X   well-intentionedr�  X   reeser�  X
   imaginabler�  X   dualr�  X   enormousr�  X   ownr�  X	   impulsiver�  X   decent-enoughr�  X   appearr�  X   thematicr�  X   un-bear-abler�  X   longr�  X   ruinousr�  X   sugaryr�  X   stoicr�  X   genial-roguer�  X   somberr�  X   grandiloquentr�  X   ugly-lookingr�  X   stylizedr�  X   involver�  X   rousingr�  X   januaryr�  X   moronicr�  X   borer�  X   glitzyr�  X   martialr�  X   disney-fiedr�  X   dr�  X   danielr�  X   junior-highr�  X	   'culturalr�  X   semi-autobiographicalr�  X
   revelatoryr�  X	   subtitledr�  X   preambler�  X   sudsyr�  X   sun-drenchedr�  X   currentr�  X   highestr�  X   m�tierr�  X   fondr�  X	   hard-corer�  X   woodenr�  X   aquaticr�  X   prostitutedr�  X   action-fantasyr�  X   pulpyr�  X	   ancillaryr�  X   'specialr�  X   unsophisticatedr�  X   lovabler�  X   stranger�  X   elderlyr�  X	   77-minuter�  X   mason-dixonr�  X   twenty-threer�  X
   insightfulr�  X   sympatheticr�  X   funny/grittyr�  X   kid-vidr�  X   dead-eyer�  X   finishr�  X   serryr�  X   11-year-oldr   X   true-to-lifer  X   catastrophicr  X   pleasantr  X
   quick-buckr  X   near-impossibler  X   salt-of-the-earthr  X   sanr  X   fairr  X	   ningu�mr	  X   action-adventurer
  X	   houseboatr  X   hecticr  X   renegade-copr  X   longerr  X   dailyr  X   genuiner  X   fleshed-outr  X   cheeser  X   never-endingr  X   high-octaner  X
   impressiver  X   plasticr  X   wearyr  X   indoctrinatedr  X   punch-drunkr  X	   four-hourr  X   smallr  X   gambler  X   long-dreadedr  X   skillfulr  X   'analyzer  X   inconceivabler   X   flaccidr!  X   well-detailedr"  X   b+r#  X   constantr$  X   zestr%  X
   near-fatalr&  X
   too-frostyr'  X	   stuffiestr(  X   rise-and-fallr)  X
   dog-paddler*  X   adorabler+  X   none-too-originalr,  X   definiter-  X   voices-from-the-other-sider.  X   purer/  X   drowsyr0  X   dubiousr1  X   belittler2  X   explicitr3  X   tabloidr4  X   corruptr5  X   homeboyr6  X   romance-novelr7  X	   palatabler8  X   old-hatr9  X   shuck-and-jiver:  X	   bloodshedr;  X   pencil-thinr<  X   wangr=  X   animalr>  X	   analgesicr?  X   fishr@  X   finerA  X   cosby-seinfeldrB  X	   broadcastrC  X   non-shakespearerD  X   unpaidrE  X   fillerrF  X   ballistic-pyrotechnicrG  X   affablerH  X   arrogantrI  X   mererJ  X   fly-on-the-wallrK  X   sugar-sweetrL  X
   dust-cakedrM  X	   strangestrN  X   pasteurizedrO  X   amped-uprP  X   lazierrQ  X
   dependablerR  X   fellowrS  X	   rapturousrT  X   pouty-lippedrU  X   inviterV  X   crazierrW  X   weirdrX  X	   sch�tterY  X   superrZ  X   struggler[  X   medalr\  X   southr]  X
   prostheticr^  X   seanr_  X   borstalr`  X   surfra  X   misanthropicrb  X   moistrc  X   listlessrd  X	   slightestre  X	   dualisticrf  X	   pointlessrg  X   softestrh  X	   yahoo-ingri  X	   sillifiedrj  X   littlerk  X   unsuccessfulrl  X   overlyrm  X   endlessrn  X   teen-orientedro  X   pee-relatedrp  X   idioticrq  X   responsiblerr  X   earlierrs  X	   72-minutert  X   time-vaultingru  X   friday-nightrv  X   'ehrw  X   trimmings�arriverx  X   deep-seatedry  X   fablerz  X   terminalr{  X   hyper-clichedr|  X   secularr}  X   distinguishr~  X   still-contemporaryr  X   infomercialr�  X   laconicr�  X	   indelibler�  X   unexplainabler�  X   capabler�  X   intentionalr�  X
   compellingr�  X   patricr�  X   bearabler�  X   open-heartedr�  X   auto-critiquer�  X   prolificr�  X   unsteadyr�  X   abysmalr�  X   rapidr�  X
   hyper-timer�  X   operaticr�  X   mysticalr�  X   half-heartedr�  X   sceneryr�  X   otherr�  X	   'renderedr�  X   fantasticalr�  X   craigr�  X   competitiver�  X   gra�asr�  X
   long-facedr�  X   undistinguishedr�  X   'right-thinkingr�  X   conan-esquer�  X	   contrivedr�  X   moldyr�  X	   memorabler�  X   bibbidy-bobbidi-blandr�  X   portent-heavyr�  X   aliver�  X   stable-fullr�  X	   vivaciousr�  X	   steamboatr�  X   interwoven�r�  X   castr�  X   longestr�  X   'chanr�  X
   undramaticr�  X   college-spawnedr�  X   lighter-than-airr�  X   hushr�  X
   big-budgetr�  X   steriler�  X   nail-bitingr�  X   less-than-compellingr�  X   writer/directorr�  X   ever-escalatingr�  X   estrogen-freer�  X   abrasiver�  X   clinicr�  X   all-americanr�  X   lighterr�  X
   historicalr�  X	   repulsiver�  X   couchr�  X   acidr�  X   wishfulr�  X   puttyr�  X   b-flickr�  X   teatralr�  X   straightr�  X   self-revealingr�  X   femininer�  X   spirit-crushingr�  X   wised-upr�  X   deleter�  X   lived-inr�  X   enabler�  X   sixthr�  X   show-bizr�  X
   pretty-boyr�  X   big-waver�  X   self-interestedr�  X
   right-wingr�  X   throat-singingr�  X   well-establishedr�  X   heftyr�  X   caper-comedyr�  X	   'comedianr�  X	   bielinskyr�  X   one-shotr�  X   intentionedr�  X   thirty-fiver�  X   control-alt-deleter�  X   contentiousr�  X   oldr�  X   somewhatr�  X   propriety-obsessedr�  X   scene-chewingr�  X   schwarzeneggerr�  X   initialr�  X	   stretchedr�  X	   syntheticr�  X	   dudsviller�  X   shiftyr�  X   tiredr�  X   distinguishabler�  X   blood-drenchedr�  X   clearr�  e(X   enterr�  X   migraine-inducingr�  X   gabbiestr�  X   duer�  X   fluidr�  X   lower-classr�  X   naturalr�  X   fatalr�  X   whateverr�  X   entertainmentr�  X   nicestr�  X   cheesyr�  X   still-inestimabler�  X   intermittentr�  X   one-dimensionalr�  X   abyssr�  X   diviner�  X   nonprofessionalr�  X
   trumped-upr�  X   anthropomorphicr�  X   riot-controlr�  X   solipsisticr�  X   shapabler�  X   so-bad-they're-goodr   X   crushedr  X   superstitiousr  X   jadedr  X   self-amusedr  X
   unoriginalr  X   medium-grader  X   frothyr  X	   seductiver  X
   first-timer	  X   simple-mindedr
  X   bumpyr  X   whiter  X   subtlerr  X   kindr  X   deadpanr  X
   unsettlingr  X	   audaciousr  X   ardentr  X   dizzyr  X   gawkyr  X   bone-chillingr  X	   real-timer  X   wide-screenr  X   rentalr  X	   incapabler  X
   sufficientr  X   evidentr  X   fatr  X   one-starr  X   burgerr  X   bisexualr  X   fleshr   X   happily-everr!  X	   exemplifyr"  X   through-liner#  X   strongr$  X   guardianr%  X   minute-by-minuter&  X   quietr'  X	   out-shockr(  X   bar-scrappingr)  X	   energeticr*  X   matchr+  X
   phenomenalr,  X   dutifulr-  X	   watchabler.  X   wishy-washyr/  X   psychedelicr0  X   revealr1  X   sixties-styler2  X
   uneventfulr3  X   nonjudgmentalr4  X   teen-targetedr5  X   cross-shapedr6  X   plainr7  X   spitefulr8  X   beneathr9  X   tanovicr:  X   sense-of-humourr;  X   faithfulr<  X   freakyr=  X   arcticr>  X   large-formatr?  X
   anti-humanr@  X   medicalrA  X	   undoubtedrB  X   big-timerC  X
   vietnameserD  X   etherealrE  X   samerF  X   ten-year-oldrG  X   cinema-besottedrH  X   unforcedrI  X   festiverJ  X	   atavisticrK  X	   vicariousrL  X   interstitialrM  X   disappearing/reappearingrN  X   pitifulrO  X	   99-minuterP  X	   veritablerQ  X	   deceptiverR  X   needyrS  X   amicablerT  X
   long-rangerU  X
   deep-sixedrV  X	   uncertainrW  X   tamb�mrX  X	   sumptuousrY  X
   well-meantrZ  X   'magnifiquer[  X   miscastr\  X   o'fallonr]  X   super-wealthyr^  X   forcingr_  X   documentaryr`  X   ultra-manipulativera  X   tonalrb  X   'post-feministrc  X   bling-blingrd  X   hard-to-believere  X
   inadequaterf  X   impishrg  X   obsessive-compulsiverh  X   clung-tori  X   outsiderj  X   spot-onrk  X   commonrl  X   movie-of-the-weekrm  X
   hawk-stylern  X	   prechewedro  X   laterp  X   low-keyrq  X   elegantrr  X   biblicalrs  X   leadenrt  X   tradition-boundru  X   long-on-the-shelfrv  X   grade-schoolrw  X   titularrx  X   dead-endry  X   self-referentialrz  X   suitedr{  X   captiver|  X   professionalr}  X   iranr~  X	   tone-deafr  X	   bilingualr�  X
   monsterousr�  X
   anti-virusr�  X   self-regardingr�  X   hideousr�  X   dangerr�  X   'greatr�  X	   junk-foodr�  X   'tronr�  X   greaterr�  X   enviousr�  X   proje��or�  X   epicr�  X
   disturbingr�  X   made-for-cabler�  X   heart-affectingr�  X   irresponsibler�  X   talkyr�  X   b�artr�  X
   compatibler�  X   super-sizedr�  X   unendurabler�  X   pleasurabler�  X   tragedyr�  X   tit-for-tatr�  X   crisperr�  X   30-yearr�  X   hollowr�  X   fosterr�  X
   half-dozenr�  X   'message-movier�  X	   patrioticr�  X	   legendaryr�  X
   ultra-loudr�  X   glacialr�  X   anti-darwinianr�  X   savvyr�  X   coeducationalr�  X   contemptibler�  X
   mid-to-lowr�  X   likelyr�  X	   obliviousr�  X	   facetiousr�  X   slimr�  X   copiousr�  X   loose-jointedr�  X   blank-facedr�  X   grittyr�  X   unimaginativer�  X   kater�  X   crime-bustingr�  X   movie-industryr�  X   straight-aheadr�  X   halr�  X   frolicr�  X   chick-flickr�  X   cluelessr�  X   unintelligibler�  X   'blackr�  X   parent-childr�  X   societalr�  X   'realr�  X   scottishr�  X   now-clichedr�  X   unconsciousr�  X   shakespearer�  X
   fragmentedr�  X   marxianr�  X   unimaginabler�  X
   humanisticr�  X   endemicr�  X   fragmentaryr�  X   gutsyr�  X	   exquisiter�  X   unwaryr�  X   �passabler�  X   uncleanr�  X   'estupendamenter�  X	   dyspepticr�  X   mockumentaryr�  X	   coma-liker�  X   jeongr�  X   quasi-shakespeareanr�  X   goose-pimpler�  X   special-effects-ladenr�  X
   disastrousr�  X	   love-hater�  X   handicappedr�  X   sadisticr�  X   spunkyr�  X   quasi-originalr�  X   overallr�  X   danter�  X   affectation-freer�  X   you-are-therer�  X   athleticr�  X   tenderr�  X   well-mountedr�  X   'lover�  X   quiter�  X   welshr�  X   fastr�  X   actoryr�  X   well-definedr�  X   high-buffedr�  X   continentalr�  X   unreachabler�  X   hard-earnedr�  X   superiorr�  X   self-importantr�  X   semi-improvisedr�  X	   imitativer�  X	   all-nightr�  X   academicr�  X   fluentr�  X   utmostr�  X   cookie-cutterr�  X
   respectfulr�  X	   satiricalr�  X   chilledr�  X   besider�  X   innumerabler�  X   flabbyr�  X	   sturdiestr�  X   lifelessr�  X	   blown-outr�  X   callowr�  X	   wonderfulr�  X   ''independentr   X	   catalyticr  X   no-bullr  X
   disposabler  X   decentr  X   super-stupidr  X   well-put-togetherr  X   knewr  X   savvierr  X   b-12r	  X
   'manhunterr
  X   trappedr  X   high-spiritedr  X   specialr  X   perfectr  X   lustr  X   coherentr  X	   expensiver  X	   favouriter  X   companionabler  X   'barbershopr  X   cynicr  X   fourthr  X   romanticr  X   adeptr  X   archiver  X   out-outrager  X   hotr  X   frontalr  X   bitchyr  X	   fanaticalr  X   arnoldr  X   liter   X   lightr!  X   convencionalr"  X   latestr#  X   action-packedr$  X   greenr%  X	   atheisticr&  X   co-starr'  X   low-calr(  X
   uproariousr)  X   pop-upr*  X   new/oldr+  X   parisianr,  X   oft-describedr-  X   claustrophicr.  X
   consistentr/  X   coldr0  X
   emblematicr1  X   eye-openingr2  X   self-discoveryr3  X   bang-upr4  X   gloriousr5  X	   lip-glossr6  X   gearr7  X   funnierr8  X   revolutionaryr9  X   rightr:  X
   fairy-taler;  X   eardrum-dicingr<  X
   disjointedr=  X   addr>  X   nicer?  X   3/4thr@  X   soul-stirringrA  X   'shouldrB  X   sluggishrC  X   kumblerD  X   so-bad-it's-funnyrE  X
   cautionaryrF  X   lousyrG  X   foul-mouthedrH  X   serviceablerI  X   'arJ  X   stupidrK  X   pressure-cookerrL  X   smarterrM  X
   redeemablerN  X   phonyrO  X   kuruptrP  X   topicalrQ  X   thinly-conceivedrR  X   well-producedrS  X   black-and-whiterT  X   missrU  X   irrepressiblerV  X   anachronisticrW  X   confrontationalrX  X   ominousrY  X   hour-and-a-halfrZ  X   word-of-mouthr[  X	   hubristicr\  X   mean-spiritedr]  X   evenr^  X   giver_  X   cheeryr`  X   verdadra  X   critic-proofrb  X   wife/colleaguerc  X   awhilerd  X   less-re  X   last-minuterf  X   demonicrg  X   heart-breakingrh  X   songri  X   hip-hoprj  X   not-so-stockrk  X
   intolerantrl  X   100-yearrm  X   unsubtlern  X
   outlandishro  X   racistrp  X
   jam-packedrq  X	   third-actrr  X   violentrs  X   page-turnerrt  X   talking-animalru  X   belly-dancingrv  X   fang-baringrw  X   challenge-hungryrx  X   considerablery  X   saucer-eyedrz  X   same-oldr{  X
   derring-dor|  X   pre-teenr}  X   magneticr~  X   autisticr  X	   hackneyedr�  X
   slap-happyr�  X   multi-layeredr�  X   tiniestr�  X   comprehensiver�  X
   imperviousr�  X   irreparabler�  X	   ballisticr�  X   neverthelessr�  X   cellularr�  X   middler�  X   character-orientedr�  X   unrealisticr�  X   rock-n-rollingr�  X   rider�  X	   northwestr�  X   straight-shootingr�  X   platinum-blonder�  X   luridr�  X   pseudo-rock-videor�  X   fairlyr�  X   self-defeatinglyr�  X   wrongr�  X
   formidabler�  X   well-contructedr�  X   what-ifr�  X   excitedr�  X   brazil-liker�  X   grievousr�  X   forestr�  X   scorseser�  X   overlongr�  X   adamantr�  X   spookyr�  X	   ex-mariner�  X   shyr�  X   retro-refittingr�  X	   'literaryr�  X   idoosyncraticr�  X
   over-blownr�  X   gang-infestedr�  X   speciousr�  X   summer-campr�  X   penr�  X   flawlessr�  X   thrift-shopr�  X   good-naturedlyr�  X   bongr�  X   manualr�  X   '70sr�  X   'butterfingeredr�  X	   well-doner�  X   pseudo-wittyr�  X
   back-storyr�  X
   three-ringr�  X   entra�abler�  X   close-upr�  X   openr�  X   cut-and-paster�  X   best-sustainedr�  X   equalr�  X
   wertmullerr�  X   paint-by-numberr�  X   tastefulr�  X   polemicr�  X   leanr�  X   teenr�  X   mildr�  X	   unchartedr�  X   unevenr�  X
   well-timedr�  X   doyler�  X	   anonymousr�  X   late-summerr�  X   fearfulr�  X   comicr�  X   high-mindedr�  X   documentary-makingr�  X   valer�  X	   teen-gangr�  X   light-footedr�  X   cynicalr�  X
   ham-fistedr�  X
   undogmaticr�  X   party-heartyr�  X   age-wiser�  X   u-boatr�  X   folksyr�  X   outragedr�  X   undevelopedr�  X   'fatalr�  X	   excessiver�  X   first-classr�  X   strong-mindedr�  X	   spielbergr�  X
   invincibler�  X   journalisticr�  X   raucousr�  X   worldly-wiser�  X   13-year-oldr�  X   damagedr�  X   co-writer/directorr�  X   thrillerr�  X   broaderr�  X   action-comedyr�  X   incompetentr�  X
   lacklusterr�  X   accumulatedr�  X   d�nder�  X   minimumr�  X   has-beenr�  X   drive-byr�  X   flimsyr�  X   avaryr�  X   black-ownedr�  X   substantiver�  X   triple-espressor�  X   blood-soakedr�  X   workabler�  X   living-roomr�  X   recognizabler�  X   smartestr�  X   computer-animatedr�  X	   notoriousr�  X
   boisterousr�  X   'chickr�  X	   well-trodr�  X   bare-midriffr�  X   shoot-'em-upr   X   'oldr  X   israeli/palestinianr  X   hunkyr  X	   slam-bangr  X   mapquestr  X
   whip-crackr  X   pecanr  X   crashr  X   thickr	  X
   arithmeticr
  X	   screwballr  X   inactiver  X   mournfulr  X   'scratchr  X
   comic-bookr  X   thirstr  X	   assertiver  X   circularr  X   transvestiter  X	   inelegantr  X	   cleverestr  X   raffishr  X   quirkyr  X	   not-quiter  X   low-rentr  X   jae-eunr  X   scherfigr  X   lifelongr  X   inappropriater  X	   clich�sr  X   buddy-comedyr  X   marriedr   X   ritualr!  X   nastyr"  X   child-rearingr#  X   passiver$  X   far-fetchedr%  X   radioactiver&  X   'anthonyr'  X   aren't-kids-cuter(  X   tinyr)  X   netr*  X   richnessr+  X
   marvellousr,  X   susceptibler-  X   republicr.  X   interchangeabler/  X
   disposibler0  X   dadaistr1  X
   uninspiredr2  X   'badr3  X   wilder4  X   half-dimensionalr5  X	   barn-sider6  X   genericr7  X   suddenr8  X   'somer9  X   strip-minedr:  X   araratr;  X   wondrousr<  X
   sex-soakedr=  X   unapologeticr>  X   'evelynr?  X   preachy-keenr@  X   bettyrA  X   -westrB  X   quixoticrC  X   boundary-hoppingrD  X   gasp-inducingrE  X   europeanrF  X   interestingrG  X   near-xenophobicrH  X   lumpyrI  X
   inevitablerJ  X   vastrK  X	   sure�ifrL  X   irishrM  X
   hedonisticrN  X   doubtfulrO  X   painfulrP  X   wildrQ  X   westrR  X   pseudo-educationalrS  X   not-so-funnyrT  X   carerU  X   cor-blimey-luv-a-duckrV  X
   protractedrW  X	   stylisticrX  X   merchandised-to-the-maxrY  X   sweetrZ  X   seasonalr[  X   co-optedr\  X
   live-honedr]  X	   art-houser^  X   punch-and-judyr_  X   doubler`  X   parapsychologicalra  X   morning-gloryrb  X   lingualrc  X   superlativerd  X   virtualre  X   motivaterf  X   old-fashioned-movierg  X	   repugnantrh  X   overmanipulativeri  X   '60srj  X   storyrk  X
   soul's-eyerl  X
   philosophyrm  X   grayishrn  X   earlyro  X   heartbeat-likerp  X	   ludicrousrq  X   sugar-coatingrr  X   intrepidrs  X   entirert  X   grooveru  X   maryrv  X
   ming-liangrw  X   disloyalrx  X	   too-tepidry  X   learntrz  X
   6-year-oldr{  X   ever-watchfulr|  X   miscellaneousr}  X
   evanescentr~  X   time-switchingr  X   domesticr�  X
   hystericalr�  X	   cognizantr�  X   regardr�  X   grenierr�  X   ignorantr�  X   teeth-clenchingr�  X   at�r�  X   socio-politicalr�  X   ill-equippedr�  X
   movie-starr�  X
   teen-speakr�  X   huge-screenr�  X   naiver�  X	   quizzicalr�  X   basicr�  X   self-assuredr�  X
   incoherentr�  X
   incendiaryr�  X   autopsyr�  X   shorer�  X
   nationwider�  X	   disbeliefr�  X   joylessr�  X   story-tellingr�  X	   solomonicr�  X   cerebralr�  X   gimmickyr�  X   directr�  X   punishr�  X   mind-bendingr�  X
   ramshackler�  X
   preferabler�  X	   bad-movier�  X   award-worthyr�  X   pur�edr�  X	   lower-witr�  X
   portentousr�  X   eyeball-to-eyeballr�  X	   politicalr�  X   d�sr�  X
   simplisticr�  X	   clean-cutr�  X
   optimisticr�  X   talentedr�  X   over-the-topr�  X   fizzler�  X   transparentr�  X   fifteen-year-oldr�  X   send-upr�  X   welcomer�  X   navel-gazingr�  X   'guyr�  X   razzle-dazzler�  X   unfunnyr�  X   routiner�  X   lifetime-channelr�  X	   repellentr�  X
   big-fistedr�  X   strung-togetherr�  X   picture-perfectr�  X   50-somethingr�  X	   low-grader�  X	   polemicalr�  X   seldhalr�  X   actualr�  X   zinger-filledr�  X   youthfulr�  X   noisyr�  X   multi-dimensionalr�  X   dannyr�  X   humaner�  X   higherr�  X   wound-lickingr�  X   artfulr�  X   foolishr�  X   soul-searchingr�  X   action/comedyr�  X
   homosexualr�  X   regalr�  X   sub-sophomoricr�  X   reactionaryr�  X   acerbicr�  X	   ineffabler�  X
   unforgivenr�  X   georgianr�  X
   australianr�  X   upbeatr�  X   comprehensibler�  X   flexibler�  X   bow-wowr�  X   middle-earthr�  X   big-bugr�  X   stealr�  X	   top-notchr�  X   dazzler�  X   freer�  X
   walled-offr�  X   ancientr�  X   dazedr�  X   sincerer�  X
   frustratedr�  X   lumpishr�  X   intellectualr�  X	   pertinentr�  X   heinousr�  X   slowestr�  X	   out-sizedr�  X   decisiver�  X   digital-effects-heavyr�  X   afloatr�  X   spring-breakr�  X   horrificr�  X   simplestr�  X   univac-liker�  X   julietr�  X   akinr�  X   americanr�  X   sweetestr�  X   invulnerabler�  X   page-turningr�  X   psychopathyr�  X   70-year-oldr�  X	   half-hourr�  X   death-defyingr�  X   characteristicr�  X   cohesiver�  X   schlockyr�  X   2-dayr   X
   late-nightr  X	   necessaryr  X   chinese-r  X   gussiedr  X   properr  X   stitchedr  X   influentialr  X
   untalentedr  X   suicider	  X	   screenfulr
  X   rehashedr  X   raggedr  X   criticalr  X
   179-minuter  X   surer  X   insignificantr  X
   perceptiver  X	   unhurriedr  X   trouble-in-the-ghettor  X   comedy-deficientr  X
   nihilisticr  X   ballsyr  X   extraordinarilyr  X   showerr  X
   candy-liker  X   -imamurar  X   leer  X   all-too-familiarr  X   'wayner  X   sublimer  X   crystalr  X   director-chefr   X   opaquer!  X
   title-boutr"  X   chineser#  X   oralr$  X	   normativer%  X   militaryr&  X   driver-esquer'  X   and-missr(  X	   objectiver)  X
   bliss-lessr*  X   flimsierr+  X   expressionisticr,  X   socio-histo-politicalr-  X   'baranr.  X   almostr/  X   toilet-humorr0  X	   video/dvdr1  X   blatantr2  X   fruitfulr3  X	   judiciousr4  X   ball-and-chainr5  X   groan-inducingr6  X   'moorer7  X	   'juveniler8  X
   servicabler9  X   fever-pitchedr:  X	   surrenderr;  X   uni-dimensionalr<  X   laugh-filledr=  X   normalr>  X   luckiestr?  X
   collectiver@  X   65thrA  X   unsympatheticrB  X   r-ratedrC  X
   sketchiestrD  X   half-formedrE  X   buoyantrF  X   funnyrG  X   brain-deadeningrH  X   manyrI  X   walkedrJ  X	   quick-cutrK  X   cablerL  X   'seekingrM  X   'toplessrN  X   hermeticrO  X   smallerrP  X
   languorousrQ  X   m-16rR  X	   adicionalrS  X   davidrT  X   atlanticrU  X   highly-praisedrV  X   griefrW  X   slam-bamrX  X   decadentrY  X
   slow-pacedrZ  X   side-by-sider[  X   bleakr\  X   atmosphericr]  X   lustrousr^  X   eye-bogglingr_  X   frighteningr`  X   piquantra  X   pedigreerb  X   rocawearrc  X   semimusicalrd  X   timidre  X   lightweightrf  X   patheticrg  X   'jackierh  X   cutting-edgeri  X   liablerj  X   nietzsche-referencingrk  X	   jacked-uprl  X   horror-comedyrm  X   olderrn  X   ineffectivero  X   ineptrp  X   almod�varrq  X   ensemblerr  X   tallrs  X	   good-timert  X
   villainousru  X   wanrv  X   na�frw  X   metaphoricalrx  X	   prescientry  X   cliche-riddledrz  X
   barbershopr{  X
   gyllenhaalr|  X   hard-drivingr}  X
   rhetoricalr~  X   deanr  X   dreadr�  X   25-centr�  X   self-empoweringr�  X   costlyr�  X
   impossibler�  X   computer-generatedr�  X   fitr�  X   toddr�  X   godawfulr�  X   downy-cheekedr�  X	   stand-offr�  X   decorousr�  X	   well-nighr�  X   'praiser�  X	   film�itr�  X	   kittenishr�  X   'easierr�  X   rambler�  X
   pedestrianr�  X   disinterestr�  X   do-overr�  X   hellishr�  X   churlishr�  X   beachedr�  X   heart-breakinglyr�  X   didacticr�  X   muddledr�  X   pithyr�  X	   virtuosicr�  X   anticipatedr�  X   mainr�  X
   stationaryr�  X   abridgedr�  X   mindlessr�  X	   one-trickr�  X
   full-blownr�  X   ethicalr�  X   media-soakedr�  X
   preemptiver�  X   imaxyr�  X   sociologicalr�  X	   mccruddenr�  X   picnicr�  X   like-themedr�  X	   feel-goodr�  X   hard-heartedr�  X   clich�-riddledr�  X
   mismatchedr�  X	   symbioticr�  X   even-handednessr�  X	   rancorousr�  X   ghoulishr�  X   �itr�  X	   set-piecer�  X
   indicativer�  X   louderr�  X   not-at-all-goodr�  X   bug-eyer�  X   prankishr�  X   white-empoweredr�  X   'snowr�  X
   ambivalentr�  X   clunkyr�  X   new-ageyr�  X   modern-officer�  X   self-possessedr�  X   burntr�  X   'backr�  X   one-sidednessr�  X   creepy-scaryr�  X
   apoliticalr�  X   'bluer�  X   super-seriousr�  X   15-year-oldr�  X   soulr�  X   sophisticatedr�  X   valley-girlr�  X   eighthr�  X   thirstyr�  X   loopyr�  X
   expressiver�  e(X   'jasonr�  X   low-heatr�  X
   talk-heavyr�  X   genre-bustingr�  X   inseparabler�  X   pompousr�  X   vacantr�  X   'blader�  X   mostr�  X   twitchyr�  X   conscientiousr�  X   free-wheelingr�  X   goofiestr�  X   skilledr�  X	   old-worldr�  X   'challengingr�  X   unusualr�  X   oscar-caliberr�  X   half-litr�  X
   impeccabler�  X   betterr�  X   rag-tagr�  X   three-minuter�  X	   heartbeatr�  X   inadvertentr�  X   employr�  X   repleter�  X   allenr�  X   controversialr�  X   opticr�  X   tuba-playingr�  X   singler�  X   satirer�  X
   delightfulr�  X   smokyr�  X   even-flowingr�  X   intactr�  X   boastr�  X   familialr�  X
   compulsiver�  X   smiler�  X   malr�  X   well-observedr�  X   'timer�  X   banter-filledr�  X   speculativer�  X
   guilt-tripr�  X   negativer   X   showyr  X   mind-numbingr  X   twohyr  X   conventionalr  X   typicalr  X   killerr  X
   articulater  X
   rapid-firer  X   canadianr	  X	   classicalr
  X   insightr  X   front-loadedr  X   worthr  X   good-heartedr  X   annualr  X   originalr  X   agnosticr  X   splendid-lookingr  X	   honorabler  X   clunk-on-the-headr  X	   unlikabler  X   all-starr  X   star-makingr  X   movie-makingr  X   'truthr  X	   code-talkr  X   distastefulr  X
   antisepticr  X	   hands-offr  X
   sophomoricr  X   missiver  X   fast-forwardr   X   natural-seemingr!  X
   electronicr"  X   waster#  X	   whimsicalr$  X   daisyr%  X   cryr&  X   pan-americanr'  X
   indistinctr(  X   disgustr)  X   meantr*  X	   overblownr+  X   tepidr,  X   anarchicr-  X   russian-r.  X   multi-characterr/  X   sucker-punchr0  X   pueriler1  X
   sugar-freer2  X   badr3  X
   hard-edgedr4  X   disney-styler5  X   rose-coloredr6  X   insularr7  X	   cutthroatr8  X   esotericr9  X
   flamboyantr:  X   moll�r;  X   entertainingr<  X   heart-rendingr=  X   down-and-dirtyr>  X	   charmlessr?  X   realizedr@  X	   immediaterA  X   organicrB  X   valedictorianrC  X   often-hilariousrD  X
   traveloguerE  X   mrrF  X   ex-wiferG  X   crossing-overrH  X   idiosyncraticrI  X   freudianrJ  X   happenrK  X   lovable-loserrL  X   haulrM  X   colorfulrN  X   sporadicrO  X
   controlledrP  X   sleep-inducingrQ  X	   reluctantrR  X   flower-powerrS  X   hangrT  X	   argentinerU  X   robinrV  X   one-jokerW  X   birthrX  X   love-struckrY  X
   unfamiliarrZ  X   passive-aggressiver[  X   fragiler\  X   turgidr]  X
   occasionalr^  X   wild-and-woollyr_  X   jaw-droppinglyr`  X	   meets-newra  X
   histrionicrb  X   small-budgetrc  X   teen-catholic-movierd  X   shallowre  X   hollywood-predictablerf  X   japanrg  X   joyfulrh  X	   spiritualri  X   warm-bloodedrj  X   musicrk  X   immaturerl  X   succinctrm  X	   freshnessrn  X   uhro  X   earnestrp  X
   archetypalrq  X	   laboriousrr  X   dark-as-pitchrs  X   retailrt  X   marginalru  X	   80-minuterv  X   magicalrw  X	   corporaterx  X	   tick-tockry  X	   detectiverz  X   bollywood/hollywoodr{  X   underdramatizedr|  X   denser}  X   well-developedr~  X   murkyr  X   bestialr�  X   'swimfanr�  X	   -stunningr�  X   decipherabler�  X   literater�  X	   flavorfulr�  X   garnishr�  X
   theatricalr�  X   eye-poppingr�  X   street-smartr�  X	   done-thatr�  X   'masterpiecer�  X
   'difficultr�  X
   euro-trashr�  X   uncharismaticr�  X   potty-mouthedr�  X   stupiderr�  X   sweaty-palmedr�  X   neo-realistr�  X	   wearisomer�  X   'mementor�  X   hat-in-handr�  X   'ethnicr�  X
   wafer-thinr�  X   dullestr�  X   helpfulr�  X   full-fledgedr�  X   clichedr�  X   bawdyr�  X   'solarisr�  X	   restraintr�  X   art-consciousr�  X   not-quite-urbanr�  X   hot-bloodedr�  X   ever-growingr�  X   screwyr�  X   greatestr�  X
   worshipfulr�  X   church-waryr�  X   'tobeyr�  X   near-futurer�  X   'spectacularr�  X   pin-liker�  X   aristocraticr�  X   radiantr�  X   fabulousr�  X   fifthr�  X   amishr�  X   leighr�  X   pedestalr�  X   stalk'n'slashr�  X   one-of-a-kindr�  X   splendidr�  X   primr�  X	   principalr�  X   usualr�  X   ya-yar�  X   eye-fillingr�  X	   gross-outr�  X   social/economic/urbanr�  X   synergisticr�  X   out-of-kilterr�  X   die-hardr�  X
   middle-ager�  X   modernr�  X   christian-themedr�  X	   primitiver�  X	   christianr�  X   hard-hittingr�  X   westernr�  X   luv-spreadingr�  X   teeny-bopperr�  X   'tilr�  X   enoughr�  X   savoryr�  X   surrealr�  X   finestr�  X   smallestr�  X   centralr�  X   gravitationalr�  X   inquisitiver�  X   massiver�  X
   crime-filmr�  X   abover�  X
   empatheticr�  X   would-ber�  X   belovedr�  X   findr�  X	   tentativer�  X	   high-techr�  X   truth-tellingr�  X   stomach-knottingr�  X   semi-stabler�  X   war-tornr�  X   hongr�  X	   avuncularr�  X   funeralr�  X   anxiousr�  X   septuagenarianr�  X   noxiousr�  X   'pocasr�  X   ill-constructedr�  X
   protectiver�  X   intermediaryr�  X   festivalr�  X   mediocrer�  X   cam'ronr�  X   tv-styler�  X   topr�  X   conflict-poweredr�  X   ledgerr�  X
   gut-busterr�  X   not-quite-deadr�  X   yellowr�  X   twinkly-eyedr�  X	   creepiestr�  X   septicr�  X   'itr�  X	   homileticr�  X
   all-aroundr�  X   certainr�  X   faciler�  X   generousr�  X   sulkyr�  X   informativer�  X   'carenter�  X   pornographicr�  X   liver�  X	   imperiousr 	  X   cluer	  X   materialr	  X   humorr	  X   lip-readingr	  X   tries-so-hard-to-be-coolr	  X   concreter	  X   handsome-lookingr	  X	   prevalentr	  X   `martinr		  X   direct-to-voidr
	  X   chloroform-soakedr	  X   culturalr	  X   greatr	  X
   gang-rapedr	  X   hazyr	  X	   loathsomer	  X   heart-rate-raisingr	  X   middle-of-the-roadr	  X
   congenitalr	  X	   geriatricr	  X   bloodyr	  X   nerve-rakedr	  X   templater	  X
   assaultiver	  X
   reassuringr	  X   fiendishr	  X   leastr	  X   crewr	  X   heart-on-its-sleever	  X   intenser	  X   majorr	  X   brattyr 	  X   unsuspensefulr!	  X   nuclearr"	  X	   rat-a-tatr#	  X   minorr$	  X   post-warr%	  X   mexicanr&	  X
   true-crimer'	  X   jealousyr(	  X   dialoguer)	  X   flick-knifer*	  X   second-rater+	  X   15-yearr,	  X   anti-establishmentr-	  X   tragicr.	  X   overduer/	  X   lightheartedr0	  X
   believabler1	  X	   reductiver2	  X   neutralr3	  X   'koreanr4	  X   africanr5	  X   portabler6	  X   clutchyr7	  X   plaguer8	  X   field-sizedr9	  X   avant-garder:	  X   two-hour-and-fifteen-minuter;	  X   all-too-humanr<	  X
   first-rater=	  X   unqualifiedr>	  X	   tolerabler?	  X   'wowr@	  X	   exemplaryrA	  X   american-russianrB	  X	   itinerantrC	  X   beyond-lamerD	  X   absoluterE	  X
   unpleasantrF	  X   money-orientedrG	  X   vacuousrH	  X   rough-traderI	  X   ill-consideredrJ	  X
   persuasiverK	  X   50-year-oldrL	  X	   downrightrM	  X
   vaudevillerN	  X
   screwed-uprO	  X	   masterfulrP	  X
   cartoonishrQ	  X   low-downrR	  X
   soderberghrS	  X   wind-in-the-hairrT	  X   flashyrU	  X   career-definingrV	  X   sensualrW	  X   beanrX	  X   quickrY	  X   disingenuousrZ	  X   hippie-turned-yuppier[	  X   rip-offr\	  X
   provincialr]	  X
   proverbialr^	  X   extraordinaryr_	  X   bestr`	  X   frankenstein-likera	  X   legalrb	  X   large-scalerc	  X   reignrd	  X   metropolitanre	  X   bairdrf	  X   talking-headrg	  X
   cumulativerh	  X
   delinquentri	  X	   executiverj	  X   goofyrk	  X   oft-brilliantrl	  X   pg-ratedrm	  X   andrewrn	  X   dearthro	  X
   suggestiverp	  X   elysianrq	  X   stunt-hungryrr	  X   mush-heartedrs	  X   perde-sert	  X
   eye-openerru	  X   handfulrv	  X   triflerw	  X   of�emotionalrx	  X   wittyry	  X   everz	  X
   outrageousr{	  X   sexualr|	  X   gallicr}	  X   well-writtenr~	  X   tendentiousr	  X   ultra-cheesyr�	  X   harshr�	  X   rhythmicr�	  X	   schmaltzyr�	  X   hard-partyingr�	  X   corniestr�	  X   ugly-ducklingr�	  X   geographicalr�	  X   idolizedr�	  X   tear-jerkingr�	  X   male-riddenr�	  X   riggedr�	  X   kangr�	  X   frankenstein-monsterr�	  X   unabler�	  X   lovedr�	  X	   estrangedr�	  X   grimyr�	  X   relicr�	  X   genrer�	  X   soddenr�	  X	   perpetualr�	  X   fantasyr�	  X   socialr�	  X	   cold-fishr�	  X   unforgettabler�	  X   bizarrer�	  X   bond-inspiredr�	  X   unflappabler�	  X	   top-heavyr�	  X   3dr�	  X   stopr�	  X   c'monr�	  X   self-dramatizingr�	  X	   satisfiedr�	  X   lesbianr�	  X   punchyr�	  X   good-badr�	  X   knuckleheadr�	  X
   contagiousr�	  X	   no-budgetr�	  X   northr�	  X   educationalr�	  X   self-centeredr�	  X	   kid-movier�	  X   ruder�	  X	   redundantr�	  X
   annie-maryr�	  X   architecturalr�	  X	   94-minuter�	  X   mundaner�	  X   working-classr�	  X   pop-influencedr�	  X   preliminaryr�	  X   interminabler�	  X   caffeinatedr�	  X   ice-tr�	  X   farcicalr�	  X   demented-funnyr�	  X   oceanr�	  X
   third-bestr�	  X   'goodfellasr�	  X   self-seriousr�	  X   impressionisticr�	  X   accomplishedr�	  X   screeching-metalr�	  X   pentecostalr�	  X   devilishr�	  X   comedy/dramar�	  X	   indulgentr�	  X   ultra-low-budgetr�	  X   murderr�	  X
   determinedr�	  X   cross-culturalr�	  X   jealousr�	  X   backr�	  X   campyr�	  X	   co-writerr�	  X	   apartheidr�	  X   grippingr�	  X   life-affirmingr�	  X   radicalr�	  X	   nonethnicr�	  X   electricr�	  X   first-timerr�	  X   distinctr�	  X   newr�	  X   post-adolescentr�	  X   uniquer�	  X   dynamiter�	  X	   lethargicr�	  X   blusterr�	  X	   once-overr�	  X   gut-clutchingr�	  X	   dishonestr�	  X   nightmarishr�	  X   nijinskyr�	  X   no-nonsenser�	  X   prevailr�	  X   'truer�	  X   spiritedr�	  X   payner�	  X	   well-shotr�	  X   richr�	  X   magicr�	  X   '50sr�	  X   devoidr�	  X   big-budget/all-starr�	  X   instantr�	  X   gender-bendingr�	  X   loveabler�	  X   'sub'-standardr�	  X   pigeonhole-resistingr�	  X   angryr�	  X   potentr�	  X   amusingr�	  X
   passionater�	  X   near-hypnoticr�	  X	   76-minuter�	  X   rip-roaringr�	  X
   faux-urbanr�	  X	   10-courser�	  X   popularr�	  X   advancedr�	  X
   portugueser�	  X   flakyr�	  X   smart-aleckr�	  X   likeabler�	  X	   not-greatr 
  X	   'grandeurr
  X
   vulnerabler
  X   hanr
  X   kongr
  X   inexpressibler
  X   poor-mer
  X   uncannyr
  X
   sociopathyr
  X   praiseworthyr	
  X
   elementaryr

  X   post-sovietr
  X
   103-minuter
  X   cannibalr
  X   decider
  X   whimsyr
  X   humorousr
  X   dumbestr
  X   peculiarr
  X   �lanr
  X	   katheriner
  X
   been-therer
  X   not-exactlyr
  X
   precociousr
  X   ill-conceivedr
  X   'sciencer
  X   famousr
  X
   figurativer
  X   loyalr
  X   variantr
  X   yawn-provokingr
  X	   schematicr
  X   bug-eyedr 
  X   decidedr!
  X	   ill-timedr"
  X   shock-you-into-laughterr#
  X
   movie�ifr$
  X   trend-hoppyr%
  X   rom-comr&
  X   small-screenr'
  X   breastr(
  X   illegalr)
  X   'sweptr*
  X   cliver+
  X   cogentr,
  X	   atrociousr-
  X   unprecedentedr.
  X   graver/
  X   well-wroughtr0
  X   commodifiedr1
  X   undeterminabler2
  X   inconsequentialr3
  X   'naturalisticr4
  X   craftyr5
  X   back-stabbingr6
  X	   attentiver7
  X   complexr8
  X
   propulsiver9
  X   swedishr:
  X	   prominentr;
  X   fewerr<
  X   positiver=
  X   affectedr>
  X   rejiggerr?
  X   family-orientedr@
  X   breezyrA
  X   fanaticrB
  X   monosyllabicrC
  X	   hierarchyrD
  X   good-deed/bad-deedrE
  X   `matrix'-stylerF
  X   inherentrG
  X   respectablerH
  X   respons�velrI
  X   fatefulrJ
  X   'toprK
  X   derisiverL
  X
   incompleterM
  X   adrianrN
  X
   auto-pilotrO
  X
   ankle-deeprP
  X   talerQ
  X   patrR
  X   'whatrS
  X   literalrT
  X   'classicrU
  X   recklessrV
  X   four-leggedrW
  X
   millennialrX
  X   self-righteousrY
  X   less-than-magicrZ
  X   strangerr[
  X   tarzanr\
  X   distinctiver]
  X   addedr^
  X   shakyr_
  X	   horrifiedr`
  X   mawkishra
  X   makeup-deeprb
  X
   gratuitousrc
  X
   admittedlyrd
  X   ironicre
  X
   well-drawnrf
  X   kid-pleasingrg
  X   uglyrh
  X	   marvelousri
  X   unfreerj
  X
   roisterousrk
  X   in-jokeyrl
  X   light-heartednessrm
  X   'triumphrn
  X   laughingro
  X
   idealisticrp
  X
   wolodarskyrq
  X	   norwegianrr
  X	   unchangedrs
  X   right-thinkingrt
  X   hyper-realisticru
  X   drug-relatedrv
  X	   continualrw
  X   plentyrx
  X   boldry
  X   world-classrz
  X   dispassionater{
  X   rambo-r|
  X   hairierr}
  X   inolvidabler~
  X   animatronicr
  X   trivialr�
  X	   pop-musicr�
  X   in-depthr�
  X	   fortifiedr�
  X   poorly-constructedr�
  X	   post-campr�
  X   self-consciousr�
  X	   ponderousr�
  X
   moralisticr�
  X   much-anticipatedr�
  X   doltishr�
  X   creature-featurer�
  X   rose-tintedr�
  X   laissez-passerr�
  X   hit-to-missr�
  X   26-year-oldr�
  X   barn-burninglyr�
  X   'bowlingr�
  X
   incredibler�
  X   crazedr�
  X   intelligentr�
  X
   mothball-yr�
  X   spaniel-eyedr�
  X   2-dr�
  X
   purposefulr�
  X   semi-humorousr�
  X   armenianr�
  X   self-mockingr�
  X   whiffle-ballr�
  X   economicr�
  X   amiabler�
  X   stress-reducingr�
  X
   unknowabler�
  X	   wholesaler�
  X   rudimentaryr�
  X   ballastr�
  X
   marketabler�
  X
   teary-eyedr�
  X   distinguishedr�
  X   understandabler�
  X   favoriter�
  X   self-destructivenessr�
  X
   undeniabler�
  X   comprensi�nr�
  X	   pasach'ker�
  X	   eccentricr�
  X   over-dramaticr�
  X   singer-turnedr�
  X   lusciousr�
  X   indian-r�
  X	   sorrowfulr�
  X   high-energyr�
  X   oddr�
  X   melodicr�
  X   pathos-filledr�
  X   trance-noirr�
  X   forgettabler�
  X   formalr�
  X   anewr�
  X   webcastr�
  X   kevinr�
  X	   importantr�
  X   frenziedr�
  X   beastr�
  X   rigorousr�
  X   high-poweredr�
  X   severer�
  X   spentr�
  X
   h�l�ner�
  X   mortalr�
  X   children's-movier�
  X   'uhhhr�
  X   northernr�
  X   imaginedr�
  X   therapeuticr�
  X   privyr�
  X   movie-specificr�
  X   youngerr�
  X
   right-handr�
  X   italianr�
  X   over-familiarityr�
  X   luckyr�
  X   wishr�
  X   low-browr�
  X   all-inclusiver�
  X	   nocturnalr�
  X	   realisticr�
  X   forthr�
  X   spectacularr�
  X	   beautifulr�
  X
   indigenousr�
  X
   old-schoolr�
  X   kiddie-orientedr�
  X   schlock-filledr�
  X   excitingr�
  X   tacticr�
  X   fudgedr�
  X   scariestr�
  X   rawr�
  X   chaoticr�
  X   totalr�
  X   struckr�
  X   veryr�
  X   grown-upr�
  X   unrepentantr�
  X   voidr�
  X   koreanr�
  X
   chabrolianr�
  X
   additionalr�
  X
   flame-liker�
  X   clich�r�
  X   cyber-horrorr�
  X   quer�
  X   herzogr�
  X   just-above-averager�
  X   bolly-hollyr�
  X	   illogicalr�
  X	   true-bluer�
  X   best-foreign-filmr�
  X   presentr�
  X   high-conceptr�
  X   mass-marketr�
  X   levantr�
  X   indispensabler�
  X	   imaginaryr�
  X   company-styler�
  X	   mich�ler�
  X   lastr�
  X   doe-eyedr   X   already-shallowr  X   police-proceduralr  X
   futuristicr  X   theologicalr  X   upsetr  X
   patronizedr  X   actingr  X   zombie-landr  X	   overpowerr	  X	   exuberantr
  X   largerr  X   'interesanter  X   snoozyr  X	   boulevardr  X   ear-splittingr  X   drasticr  X	   'bartlebyr  X   tamerr  X   -styler  X   film-culturer  X   angst-riddenr  X   horribler  X   director-writerr  X   twist-and-turnr  X   'stokedr  X
   punctuatedr  X   tutorialr  X	   road-tripr  X   wittierr  X   acting-workshopr  X	   salaciousr  X   iconicr   X   future-worldr!  X   oilyr"  X   handbag-clutchingr#  X   orleanr$  X   chewyr%  X   made-for-home-videor&  X	   differentr'  X   suerter(  X   atonalr)  X   gangster/crimer*  X   primaryr+  X   warmed-overr,  X   standardr-  X   bowel-curdlingr.  X   slow-movingr/  X	   credulousr0  X
   8-year-oldr1  X   cleanr2  X   fewr3  X   athleter4  X   sweet-and-sourr5  X   oscar-winningr6  X   worer7  X   than-likelyr8  X   goldenr9  X   gracefulr:  X   surrealisticr;  X	   sure-firer<  X   heaviestr=  X	   impassiver>  X
   methodicalr?  X
   despicabler@  X	   malleablerA  X   over-romanticizerB  X   nativerC  X   unclearrD  X   closestrE  X   ho-humrF  X   sub-tarantinorG  X
   proficientrH  X   contemptuousrI  X   bygonerJ  X   flourishrK  X   'gentlerL  X
   often-cuterM  X   clingrN  X   hatefulrO  X   zhangrP  X   non-starterrQ  X   severalrR  X   doughrS  X   long-runningrT  X   hokeyrU  X
   phlegmaticrV  X   gladrW  X   physicalrX  X   weapon-derivedrY  X   empathyrZ  X	   laid-backr[  X   walshr\  X   bergmanesquer]  X   fearlessr^  X   girlishr_  X   gothicr`  X   thriller-noirra  X   hannibalrb  X   -inevitablerc  X   wall-to-wallrd  X   anti-feministre  X   japaneserf  X
   respectiverg  X   10-year-oldrh  X   causticri  X   industrial-modelrj  X   unearthrk  X   candidrl  X   action/thrillerrm  X   psychoanalyticalrn  X
   not-nearlyro  X   three-year-oldrp  X   unfortunaterq  X   bitterrr  X   jeanrs  X
   102-minutert  X   pastru  X   'plainrv  X	   opera-ishrw  X   widerx  X   texturalry  X   publicrz  X   abler{  X   iconoclasticr|  X   franticr}  X	   on-camerar~  X   point-to-pointr  X   evilr�  X   generalr�  X
   unrealizedr�  X
   wide-angler�  X   testimonialr�  X
   unbearabler�  X	   rap-metalr�  X   boatr�  X   closed-doorr�  X	   gore-freer�  X
   unfaithfulr�  X	   cop-flickr�  X   on-boardr�  X   genre-curlingr�  X   and/orr�  X   buzz-obsessedr�  X	   entangledr�  X   partyr�  X	   dialed-upr�  X   peskyr�  X   'tonightr�  X   cozyr�  X   charmingr�  X
   monotonousr�  X	   85-minuter�  X   pimentalr�  X   two-laner�  X   visibler�  X   lingerr�  X   readr�  X   civicr�  X   resonantr�  X   romantic/comedyr�  X   honestr�  X   unpretentiousr�  X   surefirer�  X   deadr�  X   mood-alteringr�  X
   novelisticr�  X   quasi-improvisedr�  X   unambitiousr�  X   excessr�  X	   deliciousr�  X   serialr�  X   frontr�  X   out-to-change-the-worldr�  X   astonishr�  X   husband-and-wifer�  X   sugar-coatedr�  X   video-game-basedr�  X   fullerr�  X   loosey-gooseyr�  X	   real-liver�  X   'zanyr�  X   full-lengthr�  X   foreignr�  X   kennedyr�  e(X   receiver�  X   well-balancedr�  X   less-than-objectiver�  X   ryanr�  X   numerousr�  X   internalr�  X   stellarr�  X   inscrutabler�  X
   thunderousr�  X   communalr�  X   ianr�  X	   barbarianr�  X	   self-helpr�  X   academyr�  X   restrictiver�  X   appropriater�  X   ther�  X   19thr�  X   tolstoyr�  X   mideastr�  X   11thr�  X   likabler�  X	   pop-cyberr�  X	   runteldatr�  X   morer�  X
   acceptabler�  X   crowd-pleasingr�  X   blindr�  X   purposelessr�  X   teeth-gnashingr�  X   war-ravagedr�  X   giantr�  X   know-howr�  X   made-upr�  X   rigidr�  X   'tr�  X   junk-calorier�  X   independentr�  X   stereotypicalr�  X   cash-inr�  X   well-executedr�  X   behaviorr�  X   stylishr�  X   dullardr�  X   ordinaryr�  X   unlikelyr�  X   all-powerfulr�  X	   bodaciousr�  X   oscar-worthyr�  X
   staggeringr�  X   heavy-handednessr�  X   large-framer�  X	   practicalr�  X   nerve-wrackingr�  X	   chou-chour�  X	   financialr�  X
   'syntheticr�  X   shadowyr�  X   overemphaticr�  X
   'christianr�  X   obviousr�  X   progressiver�  X	   requisiter�  X   animer�  X   snappyr�  X
   cinemanticr�  X   pantomimesquer�  X   surfer-girlr�  X   -spyr�  X
   off-kilterr�  X   psychopathicr�  X   easter-egg-coloredr   X   recycledr  X   irrevocabler  X	   prettiestr  X   syr  X
   day-to-dayr  X   unifiedr  X   contemporaryr  X   slipperyr  X   unlistenabler	  X   dottedr
  X	   monstrousr  X
   low-budgetr  X   reactiver  X	   rhapsodicr  X   contemplativer  X
   date-nightr  X   standoffishr  X   two-drink-minimumr  X   circumstantialr  X   'sophisticatedr  X
   in-betweenr  X   nonagenarianr  X   detailr  X   scarierr  X   jeong-hyangr  X	   debrauwerr  X   ampler  X   jumbler  X   self-loathingr  X   nairr  X	   operativer  X	   well-wornr   X   high-profiler!  X   sticky-sweetr"  X   considerr#  X	   debatabler$  X   sensuousr%  X   steakr&  X
   half-nakedr'  X   girl-meets-girlr(  X   standard-issuer)  X   primalr*  X   contentr+  X   specificr,  X   horser-  X   neuroticr.  X   nickelodeon-esquer/  X   hospitalr0  X   unexceptionalr1  X
   surprisingr2  X   gentler3  X   silver-hairedr4  X   white-knuckledr5  X   vitalr6  X   moldr7  X   worryr8  X   giggler9  X
   rough-hewnr:  X	   one-sidedr;  X   blandr<  X   open-endednessr=  X   fine-lookingr>  X   rambunctiousr?  X   anticr@  X   pessimisticrA  X   all-over-the-maprB  X   hardrC  X   vatrD  X   free-for-allrE  X	   egregiousrF  X   hardscrabblerG  X   juniorrH  X	   halloweenrI  X   off-the-cuffrJ  X   lazyrK  X
   impersonalrL  X   uninventiverM  X   amblerN  X   implausiblerO  X   single-mindedrP  X   portugalrQ  X   toss-uprR  X
   successfulrS  X   restlessrT  X   bicentennialrU  X   daytimerV  X   shiver-inducingrW  X   all-envelopingrX  X   end-of-yearrY  X	   loneliestrZ  X   must-seer[  X   inauthenticr\  X   post-savingr]  X   six-timer^  X
   miraculousr_  X   loosely-connectedr`  X   tawdryra  X   artsyrb  X   'likerc  X   pluckyrd  X   goryre  X	   re-assessrf  X   murphyrg  X   darkrh  X   witch-styleri  X   eroticrj  X
   collateralrk  X   wererl  X   haterm  X   treacherousrn  X	   -the-cashro  X   farrp  X   confessionalrq  X   gut-wrenchingrr  X	   all-womanrs  X   best-sellingrt  X   middle-fingeredru  X
   ready-maderv  X   languidrw  X   silent-movierx  X   patchedry  X
   cutesy-pierz  X   shorterr{  X   cheesierr|  X   straight-to-videor}  X   awryr~  X   non-reactionaryr  X	   88-minuter�  X   'heyr�  X	   real-lifer�  X   verticalr�  X   biggestr�  X	   versi�nr�  X
   hot-buttonr�  X
   laser-beamr�  X   incisiver�  X	   live-wirer�  X   impenetrabler�  X   alfredr�  X	   unlimitedr�  X   drama/characterr�  X   priggishr�  X   65-year-oldr�  X   15-centr�  X   wheezyr�  X   civilr�  X   laser-projectedr�  X	   hand-heldr�  X   uneasyr�  X   defiantr�  X
   immaculater�  X   dreamyr�  X   constrictiver�  X   extendedr�  X   slightr�  X	   overnightr�  X
   deliberater�  X
   video-shotr�  X   soothingr�  X   inconsistentr�  X	   pun-ladenr�  X   biographicalr�  X   inaner�  X   thoughtfulnessr�  X   hard-bittenr�  X
   water-bornr�  X   'scoobyr�  X   guy-in-a-dressr�  X   stiff-upper-lipr�  X	   forbiddenr�  X   sillierr�  X
   ostensibler�  X   correctr�  X
   a-knockingr�  X   garishr�  X   russianr�  X   'jackassr�  X
   'realisticr�  X   irish-r�  X   scary-funnyr�  X
   enthusiastr�  X   semi-amusingr�  X   damager�  X   grumbler�  X   wretchedr�  X   ernestr�  X   testosterone-chargedr�  X   ya-yasr�  X   action-orientedr�  X   viciousr�  X
   courageousr�  X
   skate/surfr�  X   be-bopr�  X	   37-minuter�  X   oh-so-hollywoodr�  X   thriller/horrorr�  X
   too-faciler�  X   'spyr�  X   hagiographicr�  X   arguabler�  X   humanistr�  X   tie-inr�  X   off-hollywoodr�  X   star-splashedr�  X   body-switchingr�  X   cold-bloodedr�  X   cliche-drenchedr�  X   in-the-ringr�  X   temporalr�  X   string-pullingr�  X   funniestr�  X   grossr�  X   disagreeabler�  X   randomr�  X   toldr�  X
   112-minuter�  X   softr�  X   well-meaningr�  X
   voice-overr�  X   ongoingr�  X
   noteworthyr�  X   less-compellingr�  X
   100-minuter�  X
   pint-sizedr�  X   limpidr�  X   surgicalr�  X   puddler�  X   'brazilr�  X   subtlestr�  X
   fluff-ballr�  X   tianr�  X   kafka-inspiredr�  X   aloftr�  X   posibler�  X   distantr�  X	   dragonflyr�  X   fussyr�  X   stymiedr�  X   non-fanr�  X   calr�  X   well-characterizedr�  X   brodyr�  X   non-godr�  X   ill-fittingr�  X   police-orientedr�  X   sheridanr�  X   creativer�  X   bankruptr�  X   imaginaci�nr�  X   'ickr�  X	   45-minuter�  X   earthr�  X   melodramaticr�  X   armenian-canadianr�  X   hard-wonr�  X   well-conceivedr   X
   reasonabler  X   gushyr  X   a-listr  X   substance-freer  X   wince-inducingr  X   arer  X
   open-facedr  X   timelyr  X   sneakyr	  X   poorr
  X   detailedr  X   'wer  X   horridr  X	   dexterousr  X   be-all-end-allr  X   illr  X   dolorousr  X
   odd-coupler  X   merryr  X   tenser  X
   deplorabler  X   giant-screenr  X   britishr  X   questionabler  X   unattractiver  X   off-beatr  X   flippantr  X	   grittiestr  X   baader-meinhofr  X   eruptr  X   thinnestr  X   20-carr   X   um�r!  X	   'drumliner"  X   cliche-riddenr#  X   incongruousr$  X   'whor%  X
   can't-missr&  X	   defensiver'  X   futiler(  X   concealmentr)  X   hotterr*  X   down-to-earthr+  X   sandlerr,  X   betrayalr-  X   focus-groupedr.  X   majesticr/  X   adventurousr0  X	   extra-dryr1  X   unnecessaryr2  X   build-upr3  X   visualmenter4  X   eye-rollingr5  X
   survivabler6  X   olympicr7  X   viewerr8  X	   crocodiler9  X   giddyr:  X	   stupidestr;  X   compassionater<  X   cr�mer=  X   perverser>  X	   glamorousr?  X   heart-wrenchingr@  X	   sub-musicrA  X   ga-zillionthrB  X   lengthyrC  X   oafishrD  X	   toothlessrE  X   road-and-buddyrF  X   'allrG  X   lavishrH  X   abundantrI  X   half-a-dozenrJ  X   re-hashrK  X   peace-and-loverL  X   cherishrM  X   instructiverN  X   'rarerO  X   sicklyrP  X   screen-eatingrQ  X   image-mongeringrR  X   nonsensicalrS  X   sonnyrT  X   roguerU  X   exceptionalrV  X   fatalerW  X
   unfinishedrX  X   intolerablerY  X   'sacrerZ  X	   so-calledr[  X   cat-and-mouser\  X   unthinkabler]  X   dynamicr^  X	   offensiver_  X   non-bondishr`  X   juddra  X   harmlessrb  X   e-graveyardrc  X
   170-minuterd  X   full-throatedre  X   toughestrf  X   caryrg  X   linearrh  X   allegoricalri  X   uncommittedrj  X
   co-writtenrk  X   kool-aidrl  X   outrightrm  X   kerriganrn  X	   nit-pickyro  X   neo-augustinianrp  X	   soporificrq  X   steverr  X   bait-and-switchrs  X   scattershotrt  X   amoralru  X   comic-striprv  X
   uncreativerw  X   short-storyrx  X	   unnaturalry  X
   capriciousrz  X   jovialr{  X   livelierr|  X   afraidr}  X   pro-fatr~  X   obsoleter  X   appreciativer�  X
   all-frenchr�  X   'inspirationalr�  X	   propheticr�  X   uncomfortabler�  X   age-inspiredr�  X   'vainr�  X   forgiver�  X   mordantr�  X   sunsetr�  X	   catharticr�  X   unmemorabler�  X   ivanr�  X   big-namer�  X   reprehensibler�  X   ethnicr�  X   tearfulr�  X   generationalr�  X   'smallr�  X	   sensitiver�  X   weilr�  X   naryr�  X   vanr�  X   'funnyr�  X
   loquaciousr�  X
   lugubriousr�  X	   dead-eyedr�  X   italian-r�  X   hack-artistr�  X   israeli-occupiedr�  X   mystery/thrillerr�  X   intriguer�  X   gonr�  X   patriarchalr�  X
   accessibler�  X   nervousr�  X   self-pityingr�  X	   pok�monr�  X   crankyr�  X   unluckyr�  X   closer�  X   brassyr�  X   female-bondingr�  X   affectionater�  X	   not-beingr�  X   unacceptabler�  X	   invisibler�  X   well-editedr�  X   awarer�  X   heart-tuggingr�  X   weirded-r�  X
   delusionalr�  X   photographicr�  X   swimfanr�  X	   on-screenr�  X   self-styledr�  X   cheerfulr�  X
   irreverentr�  X   indomitabler�  X	   dangerousr�  X
   underlyingr�  X
   raw-nervedr�  X   shortr�  X   squarer�  X   eye-catchingr�  X   hippestr�  X   ultra-provincialr�  X   make-believer�  X	   90-minuter�  X
   basketballr�  X   gone-to-seedr�  X   establishedr�  X   ill-advisedr�  X   ill-informedr�  X   johnnyr�  X   parabler�  X   unbreakabler�  X   sedater�  X   minimalr�  X   dysfunctionalr�  X   self-consciousnessr�  X   sandalr�  X   skyscraper-trapezer�  X   /emr�  X	   attendantr�  X   girls-behaving-badlyr�  X   sweet-temperedr�  X   kinetically-chargedr�  X   racialr�  X	   innocuousr�  X   'lickr�  X   solemnr�  X   child-centeredr�  X   unfathomabler�  X   maelstr�mr�  X
   remarkabler�  X   undernourishedr�  X   direct-to-video/dvdr�  X   unbelievabler�  X   laziestr�  X	   hard-eyedr�  X	   spy-savvyr�  X	   venerabler�  X   kurdishr�  X	   near-missr�  X   cornyr�  X   comedyr�  X   40-year-oldr�  X   huger�  X   well-directedr�  X   englishr�  X   'girlsr�  X   gender-provokingr�  X   sober-mindedr�  X   scantr�  X	   shainbergr�  X   internationalr�  X   grossestr�  X   deletedr�  X   stereotypedr�  X   r�pidamenter�  X   dishr�  X   bang-the-drumr�  X   song-and-dance-manr�  X   separater�  X   maternalr�  X   50-yearr�  X
   hand-drawnr�  X   renewalr   X	   sprawlingr  X   kahnr  X
   proceduralr  X   hypocriticalr  X
   unresolvedr  X   paunchyr  X   engagedr  X   topicr  X	   unfakabler	  X   prospectiver
  X   unintentionalr  X   fleet-footedr  X
   modern-dayr  X   stripped-downr  X   'worser  X   retoldr  X
   privilegedr  X
   off-screenr  X
   comparabler  X
   'hypertimer  X   catholicr  X   12-year-oldr  X   amissr  X   techno-triper  X   litr  X   all-timer  X   insecurer  X   deviantr  X
   legitimater  X
   fictitiousr  X
   conclusiver  X   insider   X   bodyr!  X   resoluter"  X   eventfulr#  X   grandeurr$  X   inarticulater%  X   bigger-than-lifer&  X   mightyr'  X   paulr(  X   baseball-playingr)  X	   ambiguousr*  X	   'hannibalr+  X   preciser,  X   american-styler-  X   healr.  X
   'schmaltzyr/  X   problem�itr0  X   wind-tunnelr1  X   acidicr2  X   roboticr3  X   crushr4  X   spontaneousr5  X   failr6  X   endearr7  X	   flip-flopr8  X   studio-producedr9  X   highr:  X   triter;  X   slickr<  X   naipaulr=  X   technologicalr>  X   food-for-thoughtr?  X   unmentionabler@  X   biggerrA  X	   availablerB  X   soulfulrC  X   unapproachablerD  X   bone-dryrE  X   cat-and-catrF  X   localrG  X	   climacticrH  X   tardierrI  X   similarrJ  X   episodicrK  X   lump-in-the-throatrL  X	   e-mailingrM  X   unstoppablerN  X	   obsessiverO  X   same-sexrP  X   cainerQ  X   stalk-and-slashrR  X   wonderrS  X   denialrT  X   alrU  X   laudablerV  X	   incuriousrW  X   pre-shootingrX  X
   no-brainerrY  X	   smack-dabrZ  X   eastr[  X   breath-takingr\  X   indian-americanr]  X	   alcoholicr^  X   nationalr_  X   dreadedr`  X   ghostra  X   personalrb  X   inoffensiverc  X   deliberativerd  X   actorishre  X   self-hatredrf  X   'thererg  X   autocritiquerh  X   clinicalri  X
   well-oiledrj  X   headlongrk  X   g-ratedrl  X   solerm  X   blondern  X   fugitivero  X
   sex-as-warrp  X   johnrq  X   vertiginousrr  X
   delectablers  X   biopicrt  X   spyru  X   connectrv  X   porousrw  X   destroyrx  X	   elementalry  X   moldy-oldierz  X   extravagantr{  X
   seriocomicr|  X   sense-spinningr}  X   marine/legalr~  X   gelr  X	   shapelessr�  X	   hopped-upr�  X   dog-tagr�  X   i-2-spoofingr�  X   conseguer�  X   nearr�  X   deserver�  X   holographicr�  X   action-filledr�  X   less-than-thrillingr�  X
   effortlessr�  X   fast-movingr�  X   dirtyr�  X   mind-blowingr�  X   feebler�  X   disreputabler�  X   well-shapedr�  X   abruptr�  X   'franklyr�  X   nerve-rattlingr�  X   oppositer�  X   minute�withr�  X   piousr�  X   blackr�  X   cedricr�  X   late-inningr�  X
   subjectiver�  X   kidr�  X   o2-tankr�  X   directorialr�  X   off-the-rackr�  X   magicianr�  X   greasyr�  X   jerkyr�  X
   improbabler�  X   not-quite-suburbanr�  X   crazyr�  X   robustr�  X   tv-to-movier�  X   pyschologicalr�  X   'aunquer�  X   giggle-inducingr�  X   agedr�  X	   95-minuter�  X   tissue-thinr�  X   air-conditioningr�  X   -doing-it-forr�  X   bait-and-tackler�  X	   civilizedr�  X   arduousr�  X   close-to-solidr�  X   'br�  X   misogynisticr�  X   pregnantr�  X   bubbler�  X
   'linklaterr�  X   handler�  X   'letr�  X   who-wrote-shakespearer�  X   'theyr�  X   antsyr�  X   kinkyr�  X   balleticr�  X   last-secondr�  X   politer�  X   heart-stringr�  X	   clamorousr�  X   hard-pressedr�  X	   turntabler�  X   well-deservedr�  X	   deliriousr�  X   neatr�  X   disappointingr�  X   splashedr�  X
   dim-wittedr�  X
   charitabler�  X   gongr�  X   inexcusabler�  X   darkestr�  X	   sob-storyr�  X   willingr�  X   thirteen-year-oldr�  X   not-so-diviner�  X   'cherishr�  X	   fantasticr�  X   well-roundedr�  X	   delineater�  X   imageryr�  X   genialr�  X   'unfaithfulr�  X   magnificentr�  X   evangelicalr�  X   easilyr�  X   meaninglessr�  X   pretentiousr�  X   shape-shiftingr�  X
   adolescentr�  X   hostiler�  X   mam�r�  X	   75-minuter�  X   exotic-lookingr�  X
   innovativer�  X   witlessr�  X	   dewy-eyedr�  X   literaryr�  X
   three-hourr�  X
   lynch-liker�  X   bluer�  X   conversationalr�  X   am�lier�  X
   girl-powerr�  X   good-lookingr�  X   goodr�  X   beatenr�  X   occupationalr�  X   beast-withinr�  X
   metaphoricr�  X   125-yearr�  X   lastingr�  X   odoriferousr�  X   glossyr�  X   point-of-viewr�  X
   meaningfulr�  X   unpredictabler�  X   confusedr�  X   fish-out-of-waterr�  X   satanicr�  X	   physicianr�  X   joyousr   X   dearly-lovedr  X   queasyr  X   showtimer  X   bohemianr  X   sub-aquaticr  X   milquetoastr  X
   linguisticr  X   drug-inducedr  X   snake-down-the-throatr	  X   mean-spiritednessr
  X   inspirationalr  X   unimpressiver  X   clich�-ladenr  X   'diviner  X   trembler  X   verbalr  X
   irony-freer  X   warfarer  X   ladenr  X
   last-placer  X   'ifr  X	   bang-bangr  X   counter-culturalr  X	   no-frillsr  X   round-robinr  X   lackadaisicalr  X   jar-jarr  X
   exploitiver  X   self-interestr  X   expeditiousr  X   trafficr  X
   baby-facedr   X
   168-minuter!  X   secondr"  X	   plentifulr#  X   morganr$  X   scandinavianr%  X	   religiousr&  X   hyper-artificialityr'  X   herebyr(  X   high-adrenaliner)  X   susanr*  X   four-year-oldr+  X   trailer-trashr,  X   ultra-violentr-  X   governmentalr.  X   inconclusiver/  X   celeb-strewnr0  X   mild-manneredr1  X   go-for-broker2  X
   dismissiver3  X
   big-screenr4  X   janklowicz-mannr5  X   raunchyr6  X   digitalr7  X   luminousr8  X
   manga-liker9  X   lovers-on-the-runr:  X
   misty-eyedr;  X   urbanr<  X
   long-livedr=  X   shakenr>  X   cuter?  X   ill-starredr@  X   conservativerA  X
   gargantuanrB  X   animatedrC  X   upperrD  X   non-mysteryrE  X   fingerrF  X   dittyrG  X   briefrH  X   attributablerI  X
   129-minuterJ  X
   10th-graderK  X   haunted-houserL  X   comparerM  X   laterrN  X   langrO  X   done-to-deathrP  X   ho-teprQ  X   garyrR  X
   supportiverS  X   hypnoticrT  X   puritanicalrU  X   toughrV  X   self-awarenessrW  X   hard-to-predictrX  X	   essentialrY  X   unhiddenrZ  X   something-borrowedr[  X   choreographedr\  X   'comedyr]  X   well-lensedr^  X   blahr_  X
   repetitiver`  X   humor-seekingra  X	   'laughingrb  X   pseudo-sophisticatedrc  X   one-noterd  X   k-19re  X   despairrf  X   hole-riddenrg  X   caperrh  X   thrown-togetherri  X   spiralrj  X   donovanrk  X	   brand-newrl  X   human-scalerm  X   spanish-americanrn  X
   dime-storero  X   symmetricalrp  X   sock-you-in-the-eyerq  X   breathtakingrr  X   cheap-lookingrs  X	   reptilianrt  X   self-glorifiedru  X   satiricrv  X   two-dayrw  X   trx  X   open-mouthedry  X   fire-redrz  X   'boldr{  X   vietnamese-bornr|  X   stevenr}  X
   satisfyingr~  X	   eponymousr  X
   curse-freer�  X	   efficientr�  X   pay-offr�  X   joshr�  X   drearyr�  X   leanestr�  X   lillardr�  X   all-maler�  X   visualr�  X   simpler�  X
   aboriginalr�  X   20th-centuryr�  X   teenager�  X	   bona-fider�  X   unwatchabler�  X   self-indulgentr�  X
   not-so-bigr�  X   intractabler�  X   barking-madr�  X   danishr�  X   nuttyr�  X   inexpressiver�  X   thinr�  X
   style-freer�  X   cheese-lacedr�  X   uncommercialr�  X   uncinematicr�  X   mischievousr�  X	   hilariousr�  X   self-deprecatingr�  X   'justr�  X   pokyr�  X   sunnierr�  e(X	   cinematicr�  X   ages-oldr�  X   brother-manr�  X   blood-curdlingr�  X   alienr�  X   aidanr�  X   give-me-an-oscarr�  X   biter�  X
   well-actedr�  X   sleazyr�  X   logicr�  X   custom-mader�  X   port-of-callr�  X   prehistoricr�  X   subconsciousr�  X	   splat-manr�  X	   mishandler�  X   saddestr�  X   inter-familyr�  X   cliche-boundr�  X   pokemonr�  X   moralr�  X   laugh-out-loudr�  X   indigestibler�  X   heroicr�  X   earr�  X	   sedentaryr�  X   tackyr�  X   fire-breathingr�  X   placidr�  X   lushr�  X
   star-powerr�  X   meanestr�  X   bargain-basementr�  X   overstuffedr�  X
   ecologicalr�  X
   economicalr�  X   carefulr�  X   'veryr�  X   flag-wavingr�  X   based-on-truthr�  X   vapidr�  X   funr�  X   tenuousr�  X   kubrick-meets-spielbergr�  X	   admirabler�  X   soullessr�  X   cleverlyr�  X	   secretaryr�  X	   ambitiousr�  X   non-narrativer�  X   coltishr�  X   hack-and-slashr�  X   12thr�  X	   down-homer�  X   cliffhangerr�  X   cringe-inducingr�  X   goodallr�  X   subtextr�  X   14-year-oldr�  X   mixed-upr�  X
   libidinousr�  X   odorousr�  X   wingedr�  X	   potentialr�  X   neurasthenicr�  X   ruh-rohr�  X   nominalr�  X	   imposibler�  X   balancedr�  X	   trenchantr�  X   squirm-inducingr�  X   modestr�  X   stolidr�  X   self-consciouslyr�  X	   pervasiver�  X   kid-empowermentr�  X   sloppyr�  X   ideologicalr�  X	   fictionalr�  X	   follow-upr�  X   transgressiver�  X   oh-sor�  X   dull-wittedr�  X   under-rehearsedr�  X   unlaughabler�  X   classicr�  X   self-promotionr�  X	   blue-chipr�  X   drunkr�  X   'lovelyr�  X   weakestr�  X   full-bodiedr�  X   'insider�  X   destructiver�  X   rainwearr   X   better-focusedr  X   media-constructedr  X   insufferabler  X   dismalr  X   graphicr  X   little-rememberedr  X   low-lifer  X   character-basedr  X   heavyr	  X   ennui-hobbledr
  X   catchyr  X   jaw-droppingr  X   tiresomer  X   volatiler  X   fully-writtenr  X   banalr  X   scenicr  X   christmas-treer  X   previousr  X	   guessabler  X   enjoyr  X   co-operativer  X   self-indulgencer  X   again-courager  X   super-r  X   ickyr  X
   logisticalr  X   muchr  X	   addictiver  X   sitcom-worthyr  X   onlyr  X   shakespereanr   X   jenniferr!  X   'garthr"  X   freshr#  X   'godr$  X	   taiwaneser%  X	   deceitfulr&  X
   improperlyr'  X   kilt-wearingr(  X   sci-fir)  X   predictabler*  X   biler+  X   platonicr,  X	   insultingr-  X   sordidr.  X	   competentr/  X   guilt-suffusedr0  X   33-year-oldr1  X   special-interestr2  X   olivierr3  X   draggedr4  X   self-aggrandizingr5  X   disfrutabler6  X   mary-louiser7  X   instanter8  X   perryr9  X   roughr:  X	   authenticr;  X   hughr<  X   television-r=  X   'laughr>  X   self-hatingr?  X   after-schoolr@  X   emocionalmenterA  X
   recyclablerB  X   vibrantrC  X   super-simplerD  X   ego-destroyingrE  X   eventualrF  X   friendlyrG  X   english-languagerH  X   out-depressrI  X   heart-stoppingrJ  X   self-knowledgerK  X   braverL  X   unmistakablerM  X   quibblerN  X   punnyrO  X   comedianrP  X   astuterQ  X   staticrR  X   unconditionalrS  X
   artificialrT  X	   unabashedrU  X   clownishrV  X   kid-friendlyrW  X   transferralrX  X   for-fansrY  X   jean-lucrZ  X	   visionaryr[  X   quick-wittedr\  X   beachr]  X   video-viewingr^  X   ruefulr_  X   �emr`  X   mentalra  X   toxicrb  X   grantrc  X   rhetoricrd  X
   force-feedre  X   benrf  X   sensationalrg  X   adequaterh  X   passableri  X   kitschrj  X   richerrk  X   hawaiianrl  X   self-mutilatingrm  X   laddishrn  X   guiltyro  X   loverp  X   bobbedrq  X	   post-fullrr  X   hanky-pankyrs  X
   super-coolrt  X   scaryru  X   war/adventurerv  X   tastyrw  X
   minimalistrx  X   world-renownedry  X   name-callingrz  X   stomach-turningr{  X   bone-crushingr|  X   gorgeousr}  X   nakedr~  X   summerr  X   depthr�  X
   tremendousr�  X   experimentalr�  X   willfulr�  X   crackedr�  X   doggedr�  X   preachyr�  X   fun-for-fun's-saker�  X   long-sufferingr�  X   beer-fueledr�  X   waryr�  X	   tough-manr�  X   character-drivenr�  X   twenty-firstr�  X   closingr�  X   sharpr�  X	   wide-eyedr�  X   wish-fulfillingr�  X   impressionabler�  X   viler�  X
   dickensianr�  X   segalr�  X	   aestheticr�  X   peevishr�  X
   diabolicalr�  X   white-on-blackr�  X   registerr�  X   commander-in-chiefr�  X   grenobler�  X	   absurdistr�  X
   victoriousr�  X   chronicr�  X   palpabler�  X   semi-surrealistr�  X   conflictr�  X   'getr�  X'   'have-yourself-a-happy-little-holocaustr�  X   powerfulr�  X
   intriguingr�  X   truck-lovingr�  X   myopicr�  X
   flavorlessr�  X   notabler�  X   head-bangingr�  X	   weirdnessr�  X	   emotionalr�  X   travel-agencyr�  X   tried-and-truer�  X	   frightfulr�  X   cruder�  X	   chopsockyr�  X   mushyr�  X	   pussy-assr�  X   by-the-bookr�  X   valiantr�  X   egotisticalr�  X	   slapstickr�  X   gut-bustingr�  X   alongr�  X	   disparater�  X   referentialr�  X   'empowermentr�  X   cannierr�  X   bu�uelr�  X   tighterr�  X   criminalr�  X
   meets-johnr�  X   therapy-dependentr�  X   surpriser�  X
   accidentalr�  X   rockyr�  X   split-screenr�  X   'abandonr�  X   troubler�  X   sharperr�  X   -whiter�  X   conceiver�  X   metaphysicalr�  X   clearerr�  X	   bombasticr�  X   sorriestr�  X   tabler�  X   oddestr�  X   preciousr�  X   bad-luckr�  X   putridr�  X	   momentaryr�  X
   blade-thinr�  X   lessr�  X   qualr�  X   uninitiatedr�  X   averager�  X   mind-numbinglyr�  X   affirmationalr�  X   romanr�  X   ericr�  X   contextr�  X   recentr�  X   oscar-sweepingr�  X
   conceptualr�  X   non-pornr�  X	   ch�teaur�  X   emptyr�  X   dryr�  X   androidr�  X   interiorr�  X   environmentalr�  X   largest-everr�  X
   stupendousr�  X
   transplantr�  X
   humbuggeryr�  X   slackr�  X   pro-serbianr�  X   pathologicalr�  X   lucidr�  X   'redr�  X   fabricr�  X   'solidr�  X   mejorr�  X	   senselessr�  X   stuffyr�  X   grand-scaler�  X	   murderousr�  X   queenr�  X	   unemotiver�  X   animatonr�  X   paler�  X   sleep-inducinglyr�  X
   unstintingr   X   audacious-impossibler  X   forrestr  X
   laugh-freer  X   pettyr  X   unrecoverabler  X   sepia-tintedr  X   riskyr  X	   re-creater  X   visceralr	  X   18th-centuryr
  X   artisticr  X   slickestr  X   self-appointedr  X   self-containedr  X   emphaticr  X   engagingr  X   peter  X   rampantr  X	   desirabler  X   pullr  X
   ridiculousr  X
   well-honedr  X   seniorr  X   sub-formulaicr  X   hilaryr  X   smugr  X	   intrusiver  X   incomprehensibler  X   off-puttingr  X   faux-contemporaryr  X   strongerr  X   variousr   X	   satelliter!  X   terrificr"  X   co-wroter#  X   soul-strippingr$  X   integralr%  X   bungler&  X   gullibler'  X	   resentfulr(  X   ho-ho-hor)  X
   drive-thrur*  X
   breathlessr+  X   americanizedr,  X   substantialr-  X   south-of-the-borderr.  X	   well-toldr/  X   fun-seekingr0  X	   naivet�r1  X   serious-mindedr2  X   patientr3  X   edgyr4  X   sleep-deprivedr5  X   narcissisticr6  X   kookyr7  X
   auspiciousr8  X   botchedr9  X   re-friedr:  X	   adjectiver;  X   kids-in-perilr<  X   uglierr=  X   truer>  X	   consciousr?  X   fetishisticr@  X   crummy-lookingrA  X   lascivious-mindedrB  X   'liferC  X   intergalacticrD  X   feel-badrE  X	   mercenaryrF  X   lifetimerG  X   amateurrH  X
   peculiarlyrI  X   tearrJ  X   mind-destroyingrK  X	   self-maderL  X   ever-ruminatingrM  X   snorerN  X   intimaterO  X   10-inchrP  X   unessentialrQ  X   8thrR  X   picrS  X   culture-clashrT  X   attalrU  X   intentrV  X   delicaterW  X	   far-flungrX  X   festrY  X   contrastrZ  X   problematicr[  X   breakthroughr\  X   semi-coherentr]  X   journalr^  X   consciousness-raisingr_  X   waterloggedr`  X   sinisterra  X   worn-outrb  X   norrington-directedrc  X   badly-renderedrd  X	   celluloidre  X   slash-and-hackrf  X   recreationalrg  X   sassyrh  X   vaingloriousri  X   adrenalizedrj  X   stalerk  X   casualrl  X   period-piecerm  X	   excellentrn  X   watchfulro  X   'emrp  X   'whyrq  X   sentimentalrr  X   mindrs  X   compiledrt  X
   inimitableru  X   over-indulgentrv  X   surface-effectrw  X	   caucasianrx  X	   elaboratery  X   gilliganrz  X   trickyr{  X
   thoughtfulr|  X   musicalr}  X
   oppressiver~  X   goth-vampirer  X   heart-warmingr�  X   cellophane-popr�  X   teen-drivenr�  X
   fast-pacedr�  X   unhappyr�  X   formerr�  X
   bass-heavyr�  X   lunaticr�  X   post-colonialistr�  X
   threadbarer�  X   fundamentalr�  X
   well-knownr�  X   dominantr�  X	   panoramicr�  X   stiffr�  X   grimr�  X   foot-draggingr�  X	   fast-editr�  X   tediousr�  X
   all-in-allr�  X   dumbed-downr�  X   period-perfectr�  X   dumberr�  X   sturdyr�  X   thoroughr�  X   sun-splashedr�  X   often-minedr�  X   unreligiousr�  X
   convenientr�  X   variabler�  X   second-guessr�  X   fourth-rater�  X   pseudo-philosophicr�  X   terribler�  X   achronologicalr�  X   not-so-smallr�  X   creepyr�  X   shakespeareanr�  X
   discursiver�  X   family-friendlyr�  X   fabr�  X
   voluptuousr�  X   harderr�  X   foolr�  X   kenr�  X   800-pager�  X	   wholesomer�  X   smearr�  X   pop-inducedr�  X   terryr�  X	   complaintr�  X   wide-smilingr�  X   keenr�  X   dreadfulr�  X	   b�ttnerr�  X   ink-and-paintr�  X   parka-wrappedr�  X   pseudo-intellectualr�  X	   discardedr�  X   self-absorbedr�  X   dramaticr�  X   fancifulr�  X	   technicalr�  X   pie-liker�  X   one-hourr�  X	   glorifiedr�  X   jackalr�  X   fourteen-yearr�  X   exploitativer�  X   quintessentialr�  X   director/co-writerr�  X	   evocativer�  X   sixth-grader�  X   ganeshr�  X   light-heartedr�  X   life-embracingr�  X   cheekyr�  X   busyr�  X   sold-outr�  X   brothers-styler�  X   verver�  X
   candy-coatr�  X	   nightmarer�  X   driver�  X	   anti-dater�  X   freneticr�  X   insipidr�  X   pitch-perfectr�  X   campaign-trailr�  X   meanr�  X   unspeakabler�  X   drivenr�  X   anguishr�  X   tear-drenchedr�  X   life-changingr�  X   clinchr�  X   save-the-planetr�  X   realr�  X   day-oldr�  X   none-too-funnyr�  X   sweepingr�  X   irresistibler�  X   ruralr�  X   action-and-popcornr�  X   juveniler�  X
   jean-claudr�  X   queasy-stomachedr�  X   movingr�  X	   ephemeralr�  X   'menr�  X   not-too-distantr�  X
   'upliftingr�  X   jaggerr�  X   asianr�  X   brightr�  X   non-jewr�  X   brimfulr�  X	   oversizedr�  X   blairr�  X   stadium-seatr�  X   unemotionalr�  X
   open-endedr�  X   pokeyr�  X   perspectiver�  X   glacier-pacedr�  X   ineffectualr�  X   unremarkabler�  X   immortalr�  X   well-craftedr   X	   seaworthyr  X   farewell-to-innocencer  X   nosediver  X   rah-rahr  X   superfluousr  X	   wrenchingr  X   cannyr  X   torpidr  X   pushr	  X   solidr
  X	   extensiver  X   nature/nurturer  X   laughr  X   wolfishr  X   suicidalr  X   'homer  X	   tambi�nr  X	   skin-deepr  X   sanctimoniousr  X
   aggressiver  X
   persistentr  X   teachr  X   yvanr  X   blood-splatteringr  X   conradr  X   pro-wildlifer  X   unsentimentalr  X	   directiver  X   patchyr  X   selfishr  X   crappyr  X	   re-voicedr   X   straight-facedr!  X	   juicelessr"  X   fuddy-duddyr#  X   kitchen-sinkr$  X   self-infatuatedr%  X   denchr&  X   bible-studyr'  X   participantr(  X   community-therapyr)  X   comicalr*  X   wantr+  X   kathyr,  X   overly-familiarr-  X	   negotiater.  X   jump-in-your-seatr/  X
   clear-eyedr0  X   soul-killingr1  X   b-grader2  X   'der3  X
   noticeabler4  X   dullr5  X
   atmospherer6  X   dizzyingr7  X   complicatedr8  X   trashyr9  X	   secondaryr:  X   ultimater;  X
   fun-lovingr<  X	   drop-deadr=  X   moroser>  X   symbolicr?  X   'requiemr@  X   marmiterA  X   re-imaginingrB  X   institutionalizedrC  X   surviverD  X   gut-bustinglyrE  X   voyeuristicrF  X   dearrG  X	   87-minuterH  X   milerI  X   virtuousrJ  X   award-winningrK  X   illogicrL  X
   overworkedrM  X   resemblerN  X   'epicrO  X   laugh-a-minuterP  X   juliet/westrQ  X	   cheesiestrR  X   fascinatingrS  X   made-for-tvrT  X   dimensionalrU  X
   self-awarerV  X   life-at-arm's-lengthrW  X   french-producedrX  X   innocentrY  X   b-ballrZ  X   lyricalr[  X   completer\  X
   redemptiver]  X   truthfulr^  X   'arer_  X   gone-to-potr`  X   20-year-oldra  X   logicalrb  X   reality-snubbingrc  X   alternativerd  X   fresh-squeezedre  X   hopefulrf  X   prg  X   yongrh  X   two-wrongs-make-a-rightri  X   martial-artsrj  X	   war-wearyrk  X
   amateurishrl  X	   explosiverm  X   built-inrn  X   gal-palro  X
   land-basedrp  X   diaryrq  X   clumsyrr  X   melancholicrs  X   72-year-oldrt  X   'deadlyru  X   high-wattagerv  X   bio-picrw  X   tear-stainedrx  X   offalry  X   historicrz  X
   oscar-sizer{  X   touchr|  X   star-studdedr}  X   life-alteringr~  X   stomach-churningr  X   underdevelopedr�  X	   aggrievedr�  X   hide-and-seekr�  X   by-the-booksr�  X   7thr�  X
   tug-of-warr�  X   parentalr�  X	   versatiler�  X   seasonedr�  X   kung-fur�  X	   moonlightr�  X
   premi�rer�  X   offbeatr�  X   livedr�  X
   'alternater�  X
   closed-offr�  X   privater�  X
   invaluabler�  X   finishedr�  X   brittler�  X   callr�  X   half-witr�  X   crucialr�  X   healthyr�  X   nicelyr�  X	   cinephiler�  X   dementedr�  X   extantr�  X   writer-directorr�  X   'inr�  X   hour-and-a-half-longr�  X
   rock-solidr�  X	   soft-pornr�  X	   -as-nastyr�  X   comedicr�  X   flesh-and-bloodr�  X   cockyr�  X   lightestr�  X   lobotomizedr�  X   steadyr�  X   allowr�  X   touchy-feelyr�  X   trialr�  X   happyr�  X   bottom-rungr�  X   addictedr�  X   rusticr�  X   winningr�  X	   clich�dr�  X   double-pistoledr�  X   grandr�  X   shriekyr�  X   soon-to-be-forgettabler�  X   stumbler�  X   'almod�varr�  X	   kissingerr�  X
   yesteryearr�  X   squirmr�  X   touchedr�  X   courtesyr�  X   humanr�  X   wholer�  X   sadr�  X   �whatr�  X
   slash-festr�  X
   paper-thinr�  X	   lecherousr�  X   7th-centuryr�  X   spanishr�  X   biennialr�  X   mcbeal-styler�  X   coming-of-ager�  X
   commercialr�  X   full-bloodedr�  X   sketchyr�  X   inescapabler�  X   feastr�  X   sappyr�  X   decades-spanningr�  X   maudlinr�  X   discernibler�  X   bodice-ripperr�  X   flatr�  X
   meticulousr�  X
   inexorabler�  X   leakyr�  X   spy-on-the-runr�  X   unimpeachabler�  X   scandalr�  X   mini-mod-madnessr�  X
   third-rater�  X   fresh-facedr�  X	   similarlyr�  X   substitutabler�  X   poeticr�  X   aldrichr�  X   gr�  X   stand-up-comedyr�  X   splashyr�  X   east-vsr�  X   foul-naturedr�  X   brianr�  X   awfulr�  X   'openingr�  X   insensitiver�  X   adapted-r�  X   superficialr�  X	   ferociousr�  X   money-grubbingr�  X   doodler�  X   looser�  X   old-fashionedr�  X   oliverr�  X   18-year-oldr�  X   myriadr�  X
   anatomicalr�  X   gratefulr�  X   chicago-basedr�  X   show-don't-tellr�  X   na�ver�  X	   agreeabler�  X	   plausibler�  X   kasem-furnishedr�  X
   irrationalr�  X	   91-minuter�  X   chongr�  X
   predecibler�  X   benignr�  X   self-criticalr   X   curiousr  X   dead-onr  X   mythicr  X   crispr  X   european-setr  X   hands-onr  X   less-is-morer  X
   mysteriousr  X   user-friendlyr	  X   groupie/scholarr
  X	   soft-corer  X	   intricater  X   larkyr  X   high-strungr  X   even-handedr  X   tidalr  X   iranianr  X   scrappyr  X   gayr  X   dishonorabler  X   gator-bashingr  X   manipulativer  X   sirr  X   too-spectacularr  X   charismaticr  X   low-techr  X   seagalr  X	   mid-ranger  X   relevantr  X   frenchr  X	   the-looser  X	   immersiver   X   contradictoryr!  X   thought-provokingr"  X   disappointedr#  X	   life-sizer$  X   good-naturedr%  X   anti-catholicr&  X   rainyr'  X
   hit-hungryr(  X
   attachmentr)  X   sardonicr*  X   above-averager+  X   extra-larger,  X	   one-nightr-  X   feature-lengthr.  X   high-endr/  X   pseudo-seriousr0  X   prurientr1  X   weakerr2  X   interpersonalr3  X
   cheap-shotr4  X   hard-to-swallowr5  X   dry-eyedr6  X   shamefulr7  X   naturalisticr8  X   collaborativer9  X   satisfactoryr:  X
   hyper-realr;  X   guilty-pleasurer<  X   often-deadlyr=  X	   slight�r>  X   kineticr?  X	   enjoyabler@  X   furtherrA  X   wongrB  X   bigger-namerC  X   nimblerD  X   stand-uprE  X   full-frontalrF  X
   overcookedrG  X	   squeamishrH  X   smoothrI  X   unconventionalrJ  X   she-cuterK  X   lowrL  X   suchrM  X
   analyticalrN  X	   execrablerO  X
   dogma-likerP  X   blue-light-specialrQ  X   outstandingrR  X
   well-pacedrS  X
   heart-feltrT  X	   bewitchedrU  X   all-wise-guys-all-the-timerV  X   better-than-averagerW  X   kinnearrX  X   saferY  X   hitchcockianrZ  X   'llr[  X   shotr\  X	   observantr]  X	   inventiver^  X   latin-r_  X
   ellipticalr`  X   'nra  X	   unscathedrb  X   sweet-naturedrc  X
   victimizedrd  X
   -the-nightre  X   swungrf  X	   enigmaticrg  X   voc�rh  X	   woe-is-meri  X
   democraticrj  e.